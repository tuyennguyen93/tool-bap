package jp.bap.api.project;

import jp.bap.api.project.dto.*;
import jp.bap.common.exception.BadRequestException;
import jp.bap.common.exception.ResourceNotFoundException;
import jp.bap.common.response.APIResponse;
import jp.bap.common.util.SecurityUtil;
import jp.bap.common.util.constant.APIConstants;
import jp.bap.common.util.constant.MessageConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ProjectConttroller {

    @Autowired
    private ProjectService projectService;

    /**
     *
     * @param pageNO {@link Integer}
     * @param pageSize {@link Integer}
     * @param sortBy {@link Integer}
     * @return List<ProjectResponseDTO>
     */
    @GetMapping("/projects")
    public APIResponse<?> getAllProjects(@RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNO,
                                         @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize,
                                         @RequestParam(name = "sort", required = false, defaultValue = "name") String sortBy){
        if(SecurityUtil.getUser() != null)
            return APIResponse.okStatus(
                    projectService.getAllProjects(SecurityUtil.getUser().getId(), SecurityUtil.getRoleName(), pageNO, pageSize, sortBy)
            );
        return APIResponse.errorStatus(APIConstants.UNAUTHORIZED_CODE, MessageConstants.NOT_AUTHENTICATED, HttpStatus.UNAUTHORIZED);
    }

    /**
     *
     * @param id {@link Integer}
     * @return ProjectDetailsResponseDTO
     * @throws ResourceNotFoundException exception
     */
    @GetMapping("/projects/{id}")
    public APIResponse<?> getProjectDetails(@PathVariable("id") Integer id)  throws ResourceNotFoundException {
        return APIResponse.okStatus(projectService.getProjectDetails(id));
    }

    /**
     *
     * @param projectCreateRequestDTO {@link ProjectCreateRequestDTO}
     * @param bindingResult auto create
     * @return ProjectResponseDTO
     */
    @PostMapping("/projects")
    public APIResponse<?> createProject(@Valid @RequestBody ProjectCreateRequestDTO projectCreateRequestDTO,
                                        BindingResult bindingResult){
        APIResponse<?> fieldErrors = getErrorResponse(bindingResult);
        if (fieldErrors != null) return fieldErrors;
        return APIResponse.okStatus(projectService.createProject(projectCreateRequestDTO), APIConstants.CREATE_CODE);
    }

    /**
     *
     * @param id projectId
     * @return project after change status
     * @throws ResourceNotFoundException exception
     */
    @PutMapping("/projects/changeStatus/{id}")
    public APIResponse<?> changeStatusProject(@PathVariable("id") Integer id)
                                throws ResourceNotFoundException{
        return APIResponse.okStatus(projectService.changeStatusProject(id), APIConstants.SUCCESS_CODE);
    }

    @PutMapping("/projects/changeStatus")
    public APIResponse<?> changeStatusListProject(@Valid @RequestBody ProjectChangeStatusRequestDTO projectChangeStatusRequestDTO,
                                                  BindingResult bindingResult)
            throws ResourceNotFoundException{
        APIResponse<?> fieldErrors = getErrorResponse(bindingResult);
        if (fieldErrors != null) return fieldErrors;
        return APIResponse.okStatus(projectService.changeStatusListProjects(projectChangeStatusRequestDTO), APIConstants.SUCCESS_CODE);
    }

    /**
     *
     * @param id {@link Integer}
     * @return HttpStatus, code
     * @throws ResourceNotFoundException exception
     */
    @DeleteMapping("/projects/{id}")
    public APIResponse<?> deleteProject(@PathVariable("id") Integer id) throws ResourceNotFoundException {
        projectService.deleteProject(id);
        return APIResponse.okStatus(HttpStatus.FORBIDDEN, APIConstants.SUCCESS_CODE);
        /*......*/
    }

    @DeleteMapping("/projects")
    public APIResponse<?> deleteListProjects(@Valid @RequestBody ProjectIdListDTO projectIdListDTO,
                                             BindingResult bindingResult){
        APIResponse<?> fieldErrors = getErrorResponse(bindingResult);
        if (fieldErrors != null) return fieldErrors;
        return  APIResponse.okStatus(projectService.deleteListProject(projectIdListDTO.getProjectIdList()), APIConstants.SUCCESS_CODE);
    }


    /**
     *
     * @param projectParamDTO {@link ProjectParamDTO}
     * @param pageNO {@link Integer}
     * @param pageSize  {@link Integer}
     * @param sortBy {@link Integer}
     * @param bindingResult auto create
     * @return jp.bap.api.project.dto.ProjectDetailsResponseDTO
     */
    @PostMapping("/projects/search")
    public APIResponse<?> searchProject(@Valid @RequestBody ProjectParamDTO projectParamDTO,
                                        @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNO,
                                        @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize,
                                        @RequestParam(name = "sort", required = false, defaultValue = "name") String sortBy,
                                        BindingResult bindingResult){
        APIResponse<?> fieldErrors = getErrorResponse(bindingResult);
        if (fieldErrors != null) return fieldErrors;
        else if(SecurityUtil.getUser() != null)
            return APIResponse.okStatus(projectService.searchProject(projectParamDTO.getName(),
                    SecurityUtil.getUser().getId(), SecurityUtil.getRoleName(), pageNO, pageSize, sortBy));
        return APIResponse.errorStatus(APIConstants.UNAUTHORIZED_CODE, MessageConstants.NOT_AUTHENTICATED, HttpStatus.UNAUTHORIZED);
    }

    /**
     *
     * @param id {@link Integer}
     * @param projectUpdateRequestDTO {@link ProjectUpdateRequestDTO}
     * @param bindingResult auto create
     * @return ProjectResponseDTO
     */
    @PutMapping("projects/{id}")
    public APIResponse<?>  updateProject(@PathVariable( value = "id") int id,
                                         @Valid @RequestBody ProjectUpdateRequestDTO projectUpdateRequestDTO,
                                         BindingResult bindingResult)  {
        APIResponse<?> fieldErrors = getErrorResponse(bindingResult);
        if (fieldErrors != null) return fieldErrors;
        return  APIResponse.okStatus(projectService.updateProject(id, projectUpdateRequestDTO), APIConstants.SUCCESS_CODE);
    }

    @PutMapping("projects/removeMember")
    public APIResponse<?> setGetOutDateMember(@Valid @RequestBody ProjectSetGetOutDateMemberRequestDTO projectSetGetOutDateMemberRequestDTO)
                                                    throws BadRequestException {
        return APIResponse.okStatus(projectService.setGetOutDateMember(projectSetGetOutDateMemberRequestDTO), APIConstants.SUCCESS_CODE);
    }

    @PutMapping("projects/addMember")
    public APIResponse<?> addMemberToProject(@Valid @RequestBody ProjectAddMemberRequestDTO projectAddMemberRequestDTO,
                                                 BindingResult bindingResult)
                                                    throws BadRequestException {
        APIResponse<?> fieldErrors = getErrorResponse(bindingResult);
        if (fieldErrors != null) return fieldErrors;
        return APIResponse.okStatus(projectService.addMemberToProject(projectAddMemberRequestDTO), APIConstants.SUCCESS_CODE);
    }

    @GetMapping("/projects/employees/{id}")
    public APIResponse<?> getProjectByEmployeeId(@PathVariable( value = "id") Integer id,
                                                 @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNO,
                                                 @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize,
                                                 @RequestParam(name = "sort", required = false, defaultValue = "name") String sortBy){
        return  APIResponse.okStatus(projectService.getProjectByEmployeeId(id, pageNO, pageSize, sortBy), APIConstants.SUCCESS_CODE);
    }







    /**
     *
     * @param bindingResult errors
     * @return code 400 , massage
     */
    private APIResponse<?> getErrorResponse(BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            List<ErrorResponseDTO> errorResponseDTOS = new ArrayList<>();
            for(FieldError fieldError : fieldErrors){
                ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO(fieldError.getField(),
                        fieldError.getDefaultMessage());
                errorResponseDTOS.add(errorResponseDTO);
            }

            return APIResponse.errorStatus(APIConstants.BAD_REQUEST_CODE, errorResponseDTOS, HttpStatus.BAD_REQUEST);
        }
        return null;
    }
}
