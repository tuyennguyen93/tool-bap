package jp.bap.api.project.dto;

import jp.bap.common.model.Project;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class MemberProjectDTO {
    private Integer id;
    private LocalDate joinOnDate;
    private LocalDate getOutDate;
    private String memberType;
    private ProjectDTO project;
}
