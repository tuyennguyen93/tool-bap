package jp.bap.api.project.dto;

import lombok.Data;

@Data
public class ErrorResponseDTO {
    String fieldError;
    String message;

    public ErrorResponseDTO(String fieldError, String message) {
        this.fieldError = fieldError;
        this.message = message;
    }
}
