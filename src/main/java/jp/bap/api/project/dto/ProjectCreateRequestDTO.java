package jp.bap.api.project.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
public class ProjectCreateRequestDTO {


    @NotEmpty(message = "name must not empty")
    private String name;

    @NotNull(message = "startDate must not be null")
    @FutureOrPresent(message = "startDate must be Present or Future")
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate startDate;

    @NotNull(message = "endDate must not be null")
    @FutureOrPresent(message = "endDate must be Present or Future")
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate endDate;

    private String description;

    @NotNull(message = "numberOfMember must not be null")
    @PositiveOrZero(message = "numberOfMember must be positive or zero")
    private Integer numberOfMember;

    @NotNull(message = "projectManagerId must not be null")
    @PositiveOrZero(message = "projectManagerId must be positive or zero")
    private Integer projectManagerId;

    @AssertTrue(message = "EndDate must be after startDate")
    private boolean isValid(){
        if(this.endDate != null && this.startDate != null)
        return this.endDate.isAfter(this.startDate);
        return true;
    }
}
