package jp.bap.api.project.dto;

import lombok.Data;

import javax.validation.constraints.Positive;
import java.util.List;

@Data
public class ProjectSetGetOutDateMemberRequestDTO {

    @Positive(message = "employeeId must be positive")
    private Integer projectId;

    private List<Integer> employeeIdList;
}
