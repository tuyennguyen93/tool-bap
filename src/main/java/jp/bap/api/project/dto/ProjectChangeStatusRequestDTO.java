package jp.bap.api.project.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ProjectChangeStatusRequestDTO {
    @NotNull
    @NotEmpty
    List<Integer> projectIdList;

    @NotNull
    Boolean status;
}
