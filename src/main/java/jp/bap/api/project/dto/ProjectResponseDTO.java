package jp.bap.api.project.dto;

import jp.bap.common.EMEnum;
import jp.bap.common.base.BaseModel;
import jp.bap.common.model.Employee;
import jp.bap.common.model.MemberProject;
import jp.bap.common.model.Project;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectResponseDTO extends BaseModel {
    @Getter @Setter
    private String name;

    @Getter @Setter
    private LocalDate startDate;

    @Getter @Setter
    private LocalDate endDate;

    @Getter @Setter
    private String description;

    @Getter @Setter
    private EMEnum.ProjectStatus status;

    @Getter @Setter
    private Integer numberOfMember;

    @Setter
    /*@JsonIgnoreProperties({"project", "user", "joinProjects", "managerProjects"})*/
    private Employee projectManager;

    @Setter
    private List<MemberProject> memberProjects = new ArrayList<>();

    public ProjectResponseDTO(Project project) {
        BeanUtils.copyProperties(project,this);
        this.memberProjects.forEach(memberProject ->   {
            if(memberProject.getGetOutDate() == null) {
                this.members.add(new ProjectDetailsResponseDTO.MemberResponse(memberProject.getEmployeeId(),
                        memberProject.getEmployee().getFullName(),
                        memberProject.getJoinOnDate(),
                        memberProject.getGetOutDate(),
                        memberProject.getMemberType(),
                        memberProject.getEmployee().getUser().getEmail()));
            }});
    }

    public String getProjectManagerEmail() {
        return projectManager.getUser().getEmail();
    }

    @Getter @Setter
    List<ProjectDetailsResponseDTO.MemberResponse> members = new ArrayList<>();

    public List<String> getLeaders() {
        if(!memberProjects.isEmpty())
            return memberProjects.stream()
                    .filter(memberProject -> memberProject.getGetOutDate() == null && memberProject.getMemberType().equals("LEADER"))
                .map(memberProject -> memberProject.getEmployee().getUser().getEmail())
                .collect(Collectors.toList());
        return new ArrayList<>();
    }

    @Data
    public static class MemberResponse {
        private Integer employeeId;

        private String name;

        private String email;

        private LocalDate joinOnDate;

        private LocalDate getOutDate;

        private String memberType;

        public MemberResponse(Integer employeeId, String name, LocalDate joinOnDate, LocalDate getOutDate, String memberType, String email) {
            this.employeeId = employeeId;
            this.name = name;
            this.email = email;
            this.joinOnDate = joinOnDate;
            this.getOutDate = getOutDate;
            this.memberType = memberType;
        }
    }
}
