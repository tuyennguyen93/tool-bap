package jp.bap.api.project.dto;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

@Data
public class ProjectParamDTO {
    @Value("")
    private String name;
}
