package jp.bap.api.project.dto;

import jp.bap.common.EMEnum;
import jp.bap.common.base.BaseModel;
import jp.bap.common.model.Employee;
import jp.bap.common.model.MemberProject;
import jp.bap.common.model.Project;
import lombok.*;
import org.springframework.beans.BeanUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ProjectDetailsResponseDTO extends BaseModel {

    @Getter @Setter
    private String name;

    @Getter @Setter
    private LocalDate startDate;

    @Getter @Setter
    private LocalDate endDate;

    @Getter @Setter
    private String description;

    @Getter @Setter
    private EMEnum.ProjectStatus status;

    @Getter @Setter
    private Integer numberOfMember;

    @Getter @Setter
    private Integer projectManagerId;

    public String getProjectManagerEmail() {
        return projectManager.getUser().getEmail();
    }

    @Setter
   /* @JsonIgnoreProperties({"user", "level", "joinProjects", "managerProjects"})*/
    private Employee projectManager;

   /* @JsonIgnore
    @JsonIgnoreProperties(value = {"project", "employee"}, allowSetters = true)*/
    @Setter
    private List<MemberProject> memberProjects = new ArrayList<>();


    @Getter @Setter
    List<MemberResponse> members = new ArrayList<>();

    public ProjectDetailsResponseDTO(Project project) {
        BeanUtils.copyProperties(project,this);
        this.memberProjects.forEach(memberProject ->   {
            if(memberProject.getGetOutDate() == null) {
                this.members.add(new MemberResponse(memberProject.getEmployeeId(),
                        memberProject.getEmployee().getFullName(),
                        memberProject.getJoinOnDate(),
                        memberProject.getGetOutDate(),
                        memberProject.getMemberType(),
                        memberProject.getEmployee().getUser().getEmail()));
            }});
    }

    @Data
    public static class MemberResponse {
        private Integer employeeId;

        private String name;

        private String email;

        private LocalDate joinOnDate;

        private LocalDate getOutDate;

        private String memberType;

        public MemberResponse(Integer employeeId, String name, LocalDate joinOnDate, LocalDate getOutDate, String memberType, String email) {
            this.employeeId = employeeId;
            this.name = name;
            this.email = email;
            this.joinOnDate = joinOnDate;
            this.getOutDate = getOutDate;
            this.memberType = memberType;
        }
    }
}
