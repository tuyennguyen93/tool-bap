package jp.bap.api.project.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import jp.bap.common.EMEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectUpdateRequestDTO {
    private String name;

    @Positive(message = "projectManagerId must be positive")
    private Integer projectManagerId;

    private String description;

    @Temporal(value = TemporalType.TIMESTAMP)
    //@FutureOrPresent(message = "startDate must be Future or Present")
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate startDate;

    @Temporal(value = TemporalType.TIMESTAMP)
    //@FutureOrPresent(message = "endDate must be Future or Present")
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate endDate;

    @PositiveOrZero(message = "numberOfMember must be positive or zero")
    private Integer numberOfMember;


    @AssertTrue(message = "endDate must be after StartDate")
    private boolean isValidDate(){
        if(this.startDate != null && this.endDate != null)
            return this.endDate.isAfter(this.startDate);
            return true;
    }

}
