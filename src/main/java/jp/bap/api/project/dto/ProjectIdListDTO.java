package jp.bap.api.project.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class ProjectIdListDTO {
    @NotNull
    @NotEmpty
    List<Integer> projectIdList;
}
