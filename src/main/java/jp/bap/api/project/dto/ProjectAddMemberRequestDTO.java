package jp.bap.api.project.dto;

import jp.bap.common.EMEnum;
import lombok.*;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.util.List;

@Data
public class ProjectAddMemberRequestDTO {

    @Positive(message = "employeeId must be positive")
    private Integer projectId;

    private List<Member> members;

    @AssertTrue(message = "memberType is invalid")
    private boolean isValidMemberType(){
        if(members != null){
            for(Member member : this.members){
                if(!EMEnum.MemberType.contains(member.memberType))
                    return false;
            }
            return true;
        }
        return false;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Member {
        @Positive(message = "employeeId must be positive")
        private Integer employeeId;
        private String memberType;
    }
}
