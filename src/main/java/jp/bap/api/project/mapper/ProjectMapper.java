package jp.bap.api.project.mapper;


import jp.bap.api.employee.dto.EmployeeByRoleNameDTO;
import jp.bap.api.employee.dto.EmployeeResponse;
import jp.bap.api.employee.dto.SalaryDTO;
import jp.bap.api.employee.dto.TechnologyDTO;
import jp.bap.api.project.dto.MemberProjectDTO;
import jp.bap.api.project.dto.ProjectDTO;
import jp.bap.common.dto.PageInfo;
import jp.bap.common.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProjectMapper {
    public ProjectDTO getProjectDtofromModel(Project project){
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setName(project.getName());
      //  projectDTO.setStartDate(project.getStartDate());
      //  projectDTO.setEndDate(project.getEndDate());
      //  projectDTO.setDescription(project.getDescription());
        // projectDTO.setStatus(project.getStatus());
      //  projectDTO.setNumberOfMember(project.getNumberOfMember());
      //  projectDTO.setProjectManagerId(project.getProjectManagerId());

        return projectDTO;
    }

    public List<ProjectDTO> getProjectListFromModal(List<Project> projects) {
        List<ProjectDTO> lst = new ArrayList<>();
        projects.forEach(i -> {
            lst.add(getProjectDtofromModel(i));
        });
        return lst;
    }

    public MemberProjectDTO getMemberProjectDtoFromModal(MemberProject memberProject) {
        MemberProjectDTO memberProjectDTO = new MemberProjectDTO();
        memberProjectDTO.setId(memberProject.getId());
        memberProjectDTO.setJoinOnDate(memberProject.getJoinOnDate());
        memberProjectDTO.setGetOutDate(memberProject.getGetOutDate());
        memberProjectDTO.setProject(getProjectDtofromModel(memberProject.getProject()));
        return memberProjectDTO;

    }

    public List<MemberProjectDTO> getListMemberProjectDto(List<MemberProject> memberProjects){
        List<MemberProjectDTO> memberProjectDTOList = new ArrayList<>();
        memberProjects.forEach(i -> {
            memberProjectDTOList.add(getMemberProjectDtoFromModal(i));
        });
        return memberProjectDTOList;
    }

    public List<String> getListProjectDto(List<MemberProject> memberProjects){
        List<String> memberProjectDTOList = new ArrayList<>();
        memberProjects.forEach(i -> {
            memberProjectDTOList.add(i.getProject().getName());
        });
        return memberProjectDTOList;
    }

}
