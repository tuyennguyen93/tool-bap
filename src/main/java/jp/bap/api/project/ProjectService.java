package jp.bap.api.project;

import jp.bap.api.project.dto.*;
import jp.bap.common.EMEnum;
import jp.bap.common.dto.PageInfo;
import jp.bap.common.exception.BadRequestException;
import jp.bap.common.exception.PaginationSortingException;
import jp.bap.common.exception.ResourceNotFoundException;
import jp.bap.common.model.Employee;
import jp.bap.common.model.MemberProject;
import jp.bap.common.model.Project;
import jp.bap.common.repository.EmployeeRepository;
import jp.bap.common.repository.MemberProjectRepository;
import jp.bap.common.repository.ProjectRepository;
import jp.bap.common.util.constant.MessageConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private MemberProjectRepository memberProjectRepository;


    public PageInfo<?> getAllProjects(Integer userId,
                                      EMEnum.RoleName role,
                                      Integer pageNo,
                                      Integer pageSize,
                                      String sortBy){
        if(EMEnum.ProjectOrderBy.contains(sortBy)){
            Employee currentEmployee = employeeRepository.findByUserId(userId);

            if(currentEmployee == null){
                throw new ResourceNotFoundException("EMPLOYEE", "userId", userId);
            }

            Pageable pageable = PageRequest.of(pageNo -1, pageSize, Sort.by(sortBy));
            Page<Project> page;
            PageInfo<ProjectResponseDTO> pageInfo = new PageInfo<ProjectResponseDTO>();
            pageInfo.setContent(new ArrayList<>());

            switch(role)
            {
                case ROLE_ADMIN: {
                    page = projectRepository.findAll(pageable);
                    break;
                }
                case ROLE_PM : {
                    page = projectRepository.findByProjectManagerId(currentEmployee.getId(), pageable);
                    break;
                }
                case ROLE_LEADER:
                case ROLE_QCQA:
                case ROLE_MEMBER: {
                    page = projectRepository.findProjectByMemberId(currentEmployee.getId(), pageable);
                    break;
                }
                default:
                    return pageInfo;
            }

            if(page.hasContent()){
                pageInfo.setContent(page.getContent().stream().map(ProjectResponseDTO::new).collect(Collectors.toList()));
                pageInfo.setTotal(page.getTotalElements());
                pageInfo.setTotalPage(page.getTotalPages());
                pageInfo.setPageSize(page.getSize());
                pageInfo.setPage(page.getNumber());
            }

            return pageInfo;
        }else {
            throw new PaginationSortingException(MessageConstants.INVALID_CONDITION);
        }
    }

    public ProjectResponseDTO createProject(ProjectCreateRequestDTO projectCreateRequestDTO){
        /*check name*/
        if(StringUtils.isBlank(projectCreateRequestDTO.getName())){
            throw new BadRequestException(MessageConstants.PROJECT_NAME_IS_INVALID);
        }
        projectCreateRequestDTO.setName(projectCreateRequestDTO.getName().trim());
        if(projectRepository.existsByName(projectCreateRequestDTO.getName())){
            throw new BadRequestException(MessageConstants.PROJECT_NAME_EXIST);
        }
        /*check description*/
        if(projectCreateRequestDTO.getDescription() != null){
            projectCreateRequestDTO.setDescription(StringUtils.trimToNull(projectCreateRequestDTO.getDescription()));
        }
        /*check PM Is True*/
        Employee pMCheck = employeeRepository.findById(projectCreateRequestDTO.getProjectManagerId())
                            .orElseThrow(()-> new ResourceNotFoundException("Employee", "id", projectCreateRequestDTO.getProjectManagerId()));
        if(!EMEnum.RoleName.ROLE_PM.equals(pMCheck.getUser().getRole().getName())){
            throw new BadRequestException(MessageConstants.PM_NOT_FOUND);
        }
        /*saving*/
        Project  project = new Project();
        BeanUtils.copyProperties(projectCreateRequestDTO,project);
        project.setCreated(LocalDateTime.now());
        project.setStatus(EMEnum.ProjectStatus.ENABLE);
        Project updated = projectRepository.save(project);
        /*returning */
        updated.setProjectManager(employeeRepository.findById(updated.getProjectManagerId())
                .orElseThrow(()-> new ResourceNotFoundException("Project Manager", "id", updated.getProjectManagerId())));

        return new ProjectResponseDTO(updated);
    }



    public ProjectResponseDTO changeStatusProject(Integer id)
                            throws ResourceNotFoundException{
        Optional<Project> project = projectRepository.findById(id);

        if(project.isPresent()){
            Project updated = project.get();
            updated.setStatus((EMEnum.ProjectStatus.DISABLE.equals(updated.getStatus()))
                    ? EMEnum.ProjectStatus.ENABLE : EMEnum.ProjectStatus.DISABLE);
            updated.setModified(LocalDateTime.now());
            updated = projectRepository.save(updated);

            return new ProjectResponseDTO(updated);
        }
       throw new ResourceNotFoundException("Project", "id", id);
    }

    public String changeStatusListProjects(ProjectChangeStatusRequestDTO projectChangeStatusRequestDTO)
            throws ResourceNotFoundException{
        List<Integer> projectIds = projectChangeStatusRequestDTO.getProjectIdList();
        List<Integer> projectNotExisteds = new ArrayList<>();
        if(!projectIds.isEmpty()){
            projectIds.forEach(projectId -> {
                Optional<Project> project = projectRepository.findById(projectId);
                if(!project.isPresent()){
                    projectNotExisteds.add(projectId);
                }
            });
        }
        if(!projectNotExisteds.isEmpty()){
            AtomicReference<String> projectNotExisted = new AtomicReference<>(". Projects not existed with projectId: ");
            projectNotExisteds.forEach(id -> projectNotExisted.updateAndGet(v -> v + id + " ; "));
            throw new BadRequestException(projectNotExisted.get());
        }

        if(!projectIds.isEmpty() && projectNotExisteds.isEmpty()){
            projectIds.forEach(projectId -> {
                Project updated = projectRepository.findById(projectId)
                        .orElseThrow(() -> new ResourceNotFoundException("Project", "id", projectId));
                updated.setStatus(projectChangeStatusRequestDTO.getStatus()
                        ? EMEnum.ProjectStatus.ENABLE : EMEnum.ProjectStatus.DISABLE);
                updated.setModified(LocalDateTime.now());
                projectRepository.save(updated);
            });
        }

        return projectChangeStatusRequestDTO.getStatus()? "Status to ENABLE successfully" : "Status to DISABLE successfully";


    }

    public void deleteProject(Integer id) throws ResourceNotFoundException{
        Optional<Project> project = projectRepository.findById(id);
        if(project.isPresent()){
            try {
                projectRepository.deleteById(id);
            }catch (Exception e){
                throw new BadRequestException(MessageConstants.PROJECT_CANNOT_DELETE);
            }
        }
        else throw new ResourceNotFoundException("Project", "id", id);
    }

    public String deleteListProject(List<Integer> projectIdList){
        List<Integer> projectNotExisteds = new ArrayList<>();
        List<Integer> projectDeleteErrors = new ArrayList<>();
        if(!projectIdList.isEmpty()){
            projectIdList.forEach(projectId -> {
                Optional<Project> project = projectRepository.findById(projectId);
                if(!project.isPresent()){
                    projectNotExisteds.add(projectId);
                }
                else {
                    if(memberProjectRepository.existsByProjectIdAndGetOutDate(projectId, null)){
                        projectDeleteErrors.add(projectId);
                    }
                }
            });
        }
        if(!projectNotExisteds.isEmpty() && !projectDeleteErrors.isEmpty()){
            AtomicReference<String> projectNotExisted = new AtomicReference<>(". Projects not existed with projectId: ");
            AtomicReference<String> projectDeleteError = new AtomicReference<>(". Projects had members with projectId: ");
            projectNotExisteds.forEach(id -> projectNotExisted.updateAndGet(v -> v + id + " ; "));
            projectDeleteErrors.forEach(id -> projectDeleteError.updateAndGet(v -> v + id + ", "));
            throw new BadRequestException(projectNotExisted + projectDeleteError.get());
        }
        else if(!projectNotExisteds.isEmpty()){
            AtomicReference<String> projectNotExisted = new AtomicReference<>(". Projects not existed with projectId: ");
            projectNotExisteds.forEach(id -> projectNotExisted.updateAndGet(v -> v + id + " ; "));
            throw new BadRequestException(projectNotExisted.get());
        }
        else if(!projectDeleteErrors.isEmpty()){
            AtomicReference<String> projectDeleteError = new AtomicReference<>(". Projects had members with projectId: ");
            projectDeleteErrors.forEach(id -> projectDeleteError.updateAndGet(v -> v + id + " ; "));
            throw new BadRequestException(projectDeleteError.get());
        }
        if(!projectIdList.isEmpty() && projectNotExisteds.isEmpty() && projectDeleteErrors.isEmpty()){
            projectIdList.forEach(projectId -> {
                try {
                    projectRepository.deleteById(projectId);
                }catch (Exception e){
                    throw new BadRequestException(MessageConstants.PROJECT_CANNOT_DELETE);
                }
            });
        }
        return MessageConstants.PROJECTS_DELETE_SUCCESSFULLY;
    }

    @Transactional(rollbackOn = Exception.class)
    public ProjectDetailsResponseDTO updateProject(int id, ProjectUpdateRequestDTO projectClientRequest){

        /*check exist project*/
        Project project = projectRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Project", "id", id));

        /*check and set PM*/
        if(projectClientRequest.getProjectManagerId()!=null){
            Optional<Employee> pMcheck = employeeRepository.findById(projectClientRequest.getProjectManagerId());
            if(pMcheck.isPresent() && EMEnum.RoleName.ROLE_PM.equals(pMcheck.get().getUser().getRole().getName())){
                project.setProjectManager(pMcheck.get());
                project.setProjectManagerId(projectClientRequest.getProjectManagerId());
            } else throw new BadRequestException(MessageConstants.PM_NOT_FOUND);
        }
        /*check and set name*/
        projectClientRequest.setName(StringUtils.trimToNull(projectClientRequest.getName()));
        if(projectClientRequest.getName() != null){
            if(StringUtils.isBlank(projectClientRequest.getName())){
                throw new BadRequestException(MessageConstants.PROJECT_NAME_IS_INVALID);
            }
            if( !projectClientRequest.getName().equals(project.getName()) &&
                    projectRepository.existsByName(projectClientRequest.getName())){
                throw new BadRequestException(MessageConstants.PROJECT_NAME_EXIST);
        }
            project.setName(projectClientRequest.getName());
        }
        /*check and set Date*/
        if(projectClientRequest.getStartDate() != null && projectClientRequest.getEndDate() != null){
            project.setStartDate(projectClientRequest.getStartDate());
            project.setEndDate(projectClientRequest.getEndDate());
        }
        else if(projectClientRequest.getStartDate() != null && projectClientRequest.getEndDate() == null){
            if(project.getEndDate().isAfter(projectClientRequest.getStartDate())){
                project.setStartDate(projectClientRequest.getStartDate());
            }else throw new BadRequestException(MessageConstants.PROJECT_START_DATE_IS_INVALID);
        }
        else if(projectClientRequest.getStartDate() == null && projectClientRequest.getEndDate() != null){
            if(projectClientRequest.getEndDate().isAfter(project.getStartDate())){
                project.setEndDate(projectClientRequest.getEndDate());
            }else throw new BadRequestException(MessageConstants.PROJECT_END_DATE_IS_INVALID);
        }
        /*check and set description*/
        if(projectClientRequest.getDescription() != null){
            project.setDescription(StringUtils.trimToNull(projectClientRequest.getDescription()));
        }
        /*check and set number of member*/
        if(projectClientRequest.getNumberOfMember() != null){
            List<MemberProject> memberProjectCurrents = memberProjectRepository.findByProjectIdAndGetOutDate(project.getId(), null);
            if(!memberProjectCurrents.isEmpty() && projectClientRequest.getNumberOfMember() < memberProjectCurrents.size()){
                throw new BadRequestException(MessageConstants.MEMBER_PROJECT_IS_INVALID);
            }
            project.setNumberOfMember(projectClientRequest.getNumberOfMember());
        }

        /*set modified*/
        project.setModified(LocalDateTime.now());


        /*save project*/
        project = projectRepository.save(project);
        Project finalProject = project;
        /*return DTO*/
        return new ProjectDetailsResponseDTO(
                projectRepository.findById(project.getId()).orElseThrow(() -> new ResourceNotFoundException("Project", "id", finalProject.getId())));
    }

    public PageInfo<ProjectResponseDTO> searchProject(String name,
                                                      Integer userId,
                                                      EMEnum.RoleName userRole,
                                                      Integer pageNo,
                                                      Integer pageSize,
                                                      String sortBy){

        if(EMEnum.ProjectOrderBy.contains(sortBy)){

            Employee currentEmployee = employeeRepository.findByUserId(userId);

            if(currentEmployee == null){
                throw new ResourceNotFoundException("EMPLOYEE","userId",userId);
            }

            Pageable pageable = PageRequest.of(pageNo - 1, pageSize, Sort.by(sortBy));
            Page<Project> page;
            PageInfo<ProjectResponseDTO> pageInfo = new PageInfo<ProjectResponseDTO>();
            pageInfo.setContent(new ArrayList<>());


            switch(userRole)
            {
                case ROLE_ADMIN : {
                    page = projectRepository.findProjectRoleAdmin(name, pageable);
                    break;
                }
                case ROLE_PM : {
                    page = projectRepository.findProjectRolePM(name, currentEmployee.getId(), pageable);
                    break;
                }
                case ROLE_LEADER:
                case ROLE_QCQA:
                case ROLE_MEMBER: {
                    page = projectRepository.findProjectRoleMember(name, currentEmployee.getId(), pageable);
                    break;
                }
                default:
                    return pageInfo;
            }
            if(page.hasContent()){
                pageInfo.setContent(page.getContent().stream().map(ProjectResponseDTO::new).collect(Collectors.toList()));
                pageInfo.setTotal(page.getTotalElements());
                pageInfo.setTotalPage(page.getTotalPages());
                pageInfo.setPageSize(page.getSize());
                pageInfo.setPage(page.getNumber());
            }

            return pageInfo;
        }else {
            throw new PaginationSortingException(MessageConstants.INVALID_CONDITION);
        }

    }

    public ProjectDetailsResponseDTO getProjectDetails(Integer id) throws ResourceNotFoundException {

        Project project = projectRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Project","id",id));

        return new ProjectDetailsResponseDTO(project);
    }

    public PageInfo<?> getProjectByEmployeeId(Integer employeeId,
                                              Integer pageNo,
                                              Integer pageSize,
                                              String sortBy){
        if(EMEnum.ProjectOrderBy.contains(sortBy)){
            Employee employee = employeeRepository.findById(employeeId)
                    .orElseThrow(()-> new ResourceNotFoundException("Employee", "id", employeeId));

            Pageable pageable = PageRequest.of(pageNo -1, pageSize, Sort.by(sortBy));
            Page<Project> page;
            PageInfo<ProjectResponseDTO> pageInfo = new PageInfo<ProjectResponseDTO>();
            pageInfo.setContent(new ArrayList<>());

            if(EMEnum.RoleName.ROLE_ADMIN.equals(employee.getUser().getRole().getName())){
                page = projectRepository.findAll(pageable);
            }
            else if(EMEnum.RoleName.ROLE_PM.equals(employee.getUser().getRole().getName())){
                page = projectRepository.findByProjectManagerId(employeeId, pageable);
            }
            else {
                page = projectRepository.findProjectByMemberId(employeeId, pageable);
            }

            if(page.hasContent()){
                pageInfo.setContent(page.getContent().stream().map(ProjectResponseDTO::new).collect(Collectors.toList()));
                pageInfo.setTotal(page.getTotalElements());
                pageInfo.setTotalPage(page.getTotalPages());
                pageInfo.setPageSize(page.getSize());
                pageInfo.setPage(page.getNumber());
            }

            return pageInfo;
        }else {
            throw new PaginationSortingException(MessageConstants.INVALID_CONDITION);
        }
    }

    public ProjectDetailsResponseDTO setGetOutDateMember(ProjectSetGetOutDateMemberRequestDTO projectSetGetOutDateMemberRequestDTO) throws BadRequestException{
        Integer projectId = projectSetGetOutDateMemberRequestDTO.getProjectId();
        List<Integer> employeeIdList = projectSetGetOutDateMemberRequestDTO.getEmployeeIdList();
        Project project = projectRepository.findById(projectId)
                .orElseThrow(() -> new ResourceNotFoundException("Project", "id", projectId));
        if(!employeeIdList.isEmpty()){
            employeeIdList.forEach(employeeId -> {
                if(!memberProjectRepository.existsByEmployeeIdAndProjectIdAndGetOutDate(employeeId, projectId, null)){
                    throw new BadRequestException(MessageConstants.MEMBER_NOT_FOUND_IN_THIS_PROJECT + " with employeeId: " + employeeId);
                }
            });
        }

        if(!employeeIdList.isEmpty()){
            employeeIdList.forEach(employeeId -> {
                MemberProject memberProject = memberProjectRepository.findByEmployeeIdAndProjectIdAndGetOutDate(employeeId, projectId, null);
                if(memberProject != null){
                    memberProject.setGetOutDate(LocalDate.now());
                    memberProject.setModified(LocalDateTime.now());
                    memberProjectRepository.save(memberProject);
                }
            });
        }
        return new ProjectDetailsResponseDTO(
                projectRepository.findById(project.getId()).orElseThrow(() -> new ResourceNotFoundException("Project", "id", project.getId())));





    }

    public ProjectDetailsResponseDTO addMemberToProject(ProjectAddMemberRequestDTO projectAddMemberRequestDTO){
        Integer projectId = projectAddMemberRequestDTO.getProjectId();
        Project project = projectRepository.findById(projectId).orElseThrow(() -> new ResourceNotFoundException("Project", "id", projectId));

        if(!projectAddMemberRequestDTO.getMembers().isEmpty()){
            projectAddMemberRequestDTO.getMembers().forEach( member -> {
                Employee employee = employeeRepository.findById(member.getEmployeeId())
                        .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", member.getEmployeeId()));

                if(EMEnum.MemberType.LEADER.getCode().equals(member.getMemberType())
                        && !EMEnum.RoleName.ROLE_LEADER.equals(employee.getUser().getRole().getName())){
                    throw new BadRequestException(MessageConstants.LEADER_NOT_FOUND + " " + employee.getId());
                }

                if(EMEnum.RoleName.ROLE_PM.equals(employee.getUser().getRole().getName()) ||
                        EMEnum.RoleName.ROLE_ADMIN.equals(employee.getUser().getRole().getName())){
                    throw new BadRequestException(MessageConstants.ADMIN_OR_PM_CANNOT_BE_A_MEMBER_PROJECT + ", id: " + employee.getId());
                }
            });
        }

        List<MemberProject> memberProjectCurrents = memberProjectRepository.findByProjectIdAndGetOutDate(projectId, null);
        int totalNewMembers = 0;
        if(!projectAddMemberRequestDTO.getMembers().isEmpty()){
            for(ProjectAddMemberRequestDTO.Member member : projectAddMemberRequestDTO.getMembers()){
                if(!memberProjectRepository.existsByEmployeeIdAndProjectIdAndGetOutDate(member.getEmployeeId(), projectId, null)){
                    totalNewMembers = totalNewMembers + 1;
                }
            }
        }
        if(memberProjectCurrents.size() + totalNewMembers > project.getNumberOfMember()){
           throw new BadRequestException(MessageConstants.CANNOT_ADD_MEMBER_TO_PROJECT_FULL + ", numberOfMember size: " + project.getNumberOfMember()
           + ". total members current: " + memberProjectCurrents.size());
        }

        if(!projectAddMemberRequestDTO.getMembers().isEmpty()){
            projectAddMemberRequestDTO.getMembers().forEach(member -> {

                Employee employee = employeeRepository.findById(member.getEmployeeId())
                        .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", member.getEmployeeId()));

                MemberProject memberProjectExisted = memberProjectRepository.findByEmployeeIdAndProjectId(member.getEmployeeId(), projectId);

                if(memberProjectExisted != null){
                    memberProjectExisted.setMemberType(member.getMemberType());
                    memberProjectExisted.setJoinOnDate(LocalDate.now());
                    memberProjectExisted.setGetOutDate(null);
                    memberProjectExisted.setModified(LocalDateTime.now());
                    memberProjectRepository.save(memberProjectExisted);

                }else{
                    MemberProject newMemberProject = new MemberProject();
                    newMemberProject.setMemberType(member.getMemberType());
                    newMemberProject.setJoinOnDate(LocalDate.now());
                    newMemberProject.setGetOutDate(null);
                    newMemberProject.setEmployee(employee);
                    newMemberProject.setEmployeeId(employee.getId());
                    newMemberProject.setProjectId(projectId);
                    newMemberProject.setCreated(LocalDateTime.now());
                    memberProjectRepository.save(newMemberProject);
                }
            });

            return new ProjectDetailsResponseDTO(
                    projectRepository.findById(project.getId()).orElseThrow(() -> new ResourceNotFoundException("Project", "id", project.getId())));

        }
        throw new BadRequestException(MessageConstants.CANNOT_ADD_MEMBER_TO_PROJECT);

    }
}
