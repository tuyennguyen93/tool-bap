package jp.bap.api.report.dto;

import jp.bap.common.EMEnum;
import lombok.Data;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.List;

@Data
public class ReportRequestDTO {

    @NotNull(message = "reportType must be not null")
    @NotBlank(message = "reportType must be not blank")
    private String reportType;

    @FutureOrPresent(message = "dueDate must be in Future or Present")
    private LocalDate dueDate;

    @NotEmpty(message = "List employee's Id must be not empty")
    private List<Integer> employeeList;

    @AssertTrue(message = "reportType must be in format: LEADER || MEMBER || PROJECT")
    private boolean isReportType(){
        if(reportType != null)
        return EMEnum.ReportType.contains(reportType);
        return false;
    }

}
