package jp.bap.api.report.dto;

import jp.bap.common.EMEnum;
import jp.bap.common.model.Employee;
import jp.bap.common.model.Project;
import jp.bap.common.model.Report;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.time.LocalDate;

public class ReportDTO {

    @Getter @Setter
    private Integer id;

    @Getter @Setter
    private LocalDate created;

    @Getter @Setter
    private LocalDate modified;

    @Getter @Setter
    private String name;
    @Getter @Setter
    private LocalDate dueDate;
    @Getter @Setter
    private EMEnum.ReportStatus status;
    @Getter @Setter
    private Integer reportToId;
            @Setter
    private Project project;
    @Getter @Setter
    private Integer projectId;
            @Setter
    private Employee employee;
    @Getter @Setter
    private Integer employeeId;

    public String getReportType(){
        if(this.name != null){
            if (this.name.equals("MEMBER EVALUATION"))
                return "MEMBER";
            else if (this.name.equals("LEADER EVALUATION"))
                return "LEADER";
            return "PROJECT";
        }
        return null;

    }

    public ReportDTO(Report report) {
        BeanUtils.copyProperties(report, this);
        this.created = LocalDate.from(report.getCreated());
        this.modified = LocalDate.from(report.getModified());
    }

    public String getProjectName() {
        return project.getName();
    }

    public String getFullName() {
        return employee.getFullName();
    }

    public String getEmail() {
        return employee.getUser().getEmail();
    }

    public EMEnum.RoleName getRole(){
        return employee.getUser().getRole().getName();
    }
}
