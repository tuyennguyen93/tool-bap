package jp.bap.api.report.dto;

import jp.bap.common.EMEnum;
import jp.bap.common.base.BaseModel;
import jp.bap.common.model.Employee;
import jp.bap.common.model.Project;
import jp.bap.common.model.ReportDetail;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EvalutionDTO extends BaseModel {

    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private LocalDate dueDate;
    @Getter
    @Setter
    private EMEnum.ReportStatus status;
    @Getter
    @Setter
    private Integer reportToId;
    @Setter
    private Project project;
    @Getter
    @Setter
    private Integer projectId;
    @Setter
    private Employee employee;
    @Getter
    @Setter
    private Integer employeeId;
    @Setter
    private List<ReportDetail> reportDetails = new ArrayList<>();

    public String getProjectName() {
        return project.getName();
    }

    public String getFullName() {
        return employee.getFullName();
    }

    public String getEmail() {
        return employee.getUser().getEmail();
    }

    public String getReportType() {
        if (this.name != null) {
            if (this.name.equals("MEMBER EVALUATION"))
                return "MEMBER";
            else if (this.name.equals("LEADER EVALUATION"))
                return "LEADER";
            return "PROJECT";
        }
        return null;
    }

    public List<?> getReportDetails() {
        if (!reportDetails.isEmpty()) {
            switch (this.name) {
                case "LEADER EVALUATION": {
                    Double total = reportDetails.stream()
                            .mapToDouble(reportDetail ->
                                    ((double) reportDetail.getCriteria().getWeight()) / 100.0 * reportDetail.getEvaluation()
                            ).sum();
                    List<LeaderEvaluation> leaderEvaluations = this.reportDetails.stream().map(LeaderEvaluation::new)
                            .collect(Collectors.toList());
                    leaderEvaluations.add(new LeaderEvaluation("Total", total));
                    return leaderEvaluations;
                }
                case "MEMBER EVALUATION": {

                    Double average = reportDetails.stream()
                            .mapToDouble(reportDetail -> reportDetail.getEvaluation())
                            .average()
                            .orElse(Double.NaN);

                    List<MemberEvaluation> memberEvaluations = this.reportDetails.stream().map(MemberEvaluation::new).collect(Collectors.toList());
                    memberEvaluations.add(new MemberEvaluation("Avg", average));
                    return memberEvaluations;
                }
                case "PROJECT EVALUATION": {

                    Double average = reportDetails.stream()
                            .mapToDouble(reportDetail -> reportDetail.getEvaluation())
                            .average()
                            .orElse(Double.NaN);

                    List<ProjectEvaluation> projectEvaluations = this.reportDetails.stream().map(ProjectEvaluation::new).collect(Collectors.toList());
                    projectEvaluations.add(new ProjectEvaluation("Avg", average));
                    return projectEvaluations;
                }
                default:
                    return new ArrayList<>();
            }
        }

        return new ArrayList<>();
    }

    @Data
    public static class LeaderEvaluation {
        private Integer id;
        private String name;
        private String description;
        private Integer weight;
        private Double evaluation;
        private String comment;

        public LeaderEvaluation(ReportDetail reportDetail) {
            BeanUtils.copyProperties(reportDetail, this);
            this.id = reportDetail.getId();
            this.name = reportDetail.getCriteria().getName();
            this.description = reportDetail.getCriteria().getDescription();
            this.weight = reportDetail.getCriteria().getWeight();
            this.evaluation = reportDetail.getEvaluation().doubleValue();
            this.comment = reportDetail.getComment();
        }

        public LeaderEvaluation(String total, Double value) {
            this.name = total;
            this.evaluation = value;
        }
    }

    @Data
    public static class MemberEvaluation {
        private Integer id;
        private String name;
        private Double evaluation;


        public MemberEvaluation(ReportDetail reportDetail) {
            BeanUtils.copyProperties(reportDetail, this);
            this.id = reportDetail.getId();
            this.name = reportDetail.getCriteria().getName();
            this.evaluation = reportDetail.getEvaluation().doubleValue();
        }

        public MemberEvaluation(String average, Double value) {
            this.name = average;
            this.evaluation = value;
        }
    }

    @Data
    public static class ProjectEvaluation {
        private Integer id;
        private String name;
        private Double evaluation;

        public ProjectEvaluation(ReportDetail reportDetail) {
            BeanUtils.copyProperties(reportDetail, this);
            this.id = reportDetail.getId();
            this.name = reportDetail.getCriteria().getName();
            this.evaluation = reportDetail.getEvaluation().doubleValue();
        }

        public ProjectEvaluation(String average, Double value) {
            this.name = average;
            this.evaluation = value;
        }
    }
}