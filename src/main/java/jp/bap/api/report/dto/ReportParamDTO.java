package jp.bap.api.report.dto;

import lombok.Data;


@Data
public class ReportParamDTO {
    private String reportStatus;
}
