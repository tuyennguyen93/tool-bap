package jp.bap.api.report.dto;

import lombok.Data;

@Data
public class ReportSearchParamDto {
    private String fullName;
    private String email;
    private String projectName;
}
