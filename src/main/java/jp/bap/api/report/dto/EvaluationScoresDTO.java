package jp.bap.api.report.dto;

import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EvaluationScoresDTO {

    @NotNull(message = "criteriaId must not empty")
    private Integer criteriaId;

    @Max(10)
    @PositiveOrZero
    private Double evaluation;

    private String comment;
}
