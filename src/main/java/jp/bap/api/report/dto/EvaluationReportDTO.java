package jp.bap.api.report.dto;

import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EvaluationReportDTO {

    @NotNull(message = "reportId must not empty")
    private Integer reportId;

    @Valid
    private List<EvaluationScoresDTO> criteriaDTOs;
}
