package jp.bap.api.report.dto;

import lombok.Data;

@Data
public class ReportResponseDTO {
    private String message;

    public ReportResponseDTO(String message) {
        this.message = message;
    }
}
