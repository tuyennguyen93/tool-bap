package jp.bap.api.report;

import jp.bap.api.report.dto.EvaluationScoresDTO;
import jp.bap.api.report.dto.EvaluationReportDTO;
import jp.bap.api.report.dto.ReportRequestDTO;
import jp.bap.api.report.dto.ReportResponseDTO;
import jp.bap.api.employee.dto.EmployeeResponse;
import jp.bap.api.project.dto.ProjectResponseDTO;
import jp.bap.api.report.dto.*;
import jp.bap.common.EMEnum;
import jp.bap.common.dto.ApiStatusDto;
import jp.bap.common.dto.PageInfo;
import jp.bap.common.exception.BadRequestException;
import jp.bap.common.exception.PaginationSortingException;
import jp.bap.common.exception.ResourceNotFoundException;
import jp.bap.common.model.*;
import jp.bap.common.repository.*;
import jp.bap.common.util.constant.APIConstants;
import jp.bap.common.util.constant.MessageConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Optional;

@Service
public class ReportService {

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private CriteriaRepository criteriaRepository;

    @Autowired
    private ReportDetailRepository reportDetailRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private MemberProjectRepository memberProjectRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    public ReportResponseDTO createReportRequest(ReportRequestDTO reportRequestDTO) {

        List<Criteria> criterias = criteriaRepository.findAllByReportType(reportRequestDTO.getReportType());

        /*List EMPLOYEEs do report*/
        List<Integer> employeeIds = reportRequestDTO.getEmployeeList();

        /*validation*/
        if (!employeeIds.isEmpty()) {
            employeeIds.forEach(employeeId -> {
                Employee employee = employeeRepository.findById(employeeId)
                        .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", employeeId));
                if (EMEnum.RoleName.ROLE_MEMBER.equals(employee.getUser().getRole().getName()) &&
                        EMEnum.ReportType.MEMBER.getCode().equals(reportRequestDTO.getReportType())) {
                    throw new BadRequestException(MessageConstants.MEMBER_NOT_EVALUATION_MEMBER);
                }
                if (EMEnum.RoleName.ROLE_PM.equals(employee.getUser().getRole().getName()) &&
                        EMEnum.ReportType.LEADER.getCode().equals(reportRequestDTO.getReportType())) {
                    throw new BadRequestException(MessageConstants.PM_NOT_EVALUATION_LEADER);
                }
            });
        }

        /*Create Reports*/
        switch (reportRequestDTO.getReportType()) {
            /*REPORT_TYPE is Leader Evaluation*/
            case "LEADER": {
                return initReportLeader(employeeIds, criterias, reportRequestDTO.getDueDate());
            }
            /*REPORT_TYPE is Member Evaluation*/
            case "MEMBER": {
                return initReportMember(employeeIds, criterias, reportRequestDTO.getDueDate());
            }
            /*REPORT_TYPE is Project Evaluation*/
            case "PROJECT": {
                return initReportProject(employeeIds, criterias, reportRequestDTO.getDueDate());
            }
            default:
                throw new BadRequestException(MessageConstants.REPORTS_CAN_NOT_INIT);
        }
    }

    public PageInfo<?> getAllReportsByAdmin(ReportParamDTO reportParamDTO,
                                            Integer pageNo,
                                            Integer pageSize,
                                            String sortBy) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, Sort.by(sortBy));
        PageInfo<ReportDTO> pageInfo = new PageInfo<>();
        pageInfo.setContent(new ArrayList<>());
        Page<Report> page;
        if (EMEnum.ReportOrderBy.contains(sortBy)) {
            /*reportStatus is valid*/
            if (reportParamDTO != null && reportParamDTO.getReportStatus() != null &&
                    EMEnum.ReportStatus.contains(reportParamDTO.getReportStatus())) {
                page = reportRepository.findAllByStatus(EMEnum.ReportStatus.valueOf(reportParamDTO.getReportStatus()), pageable);
                return getPageInfo(pageInfo, page);

            } else if (reportParamDTO != null && (StringUtils.isBlank(reportParamDTO.getReportStatus()) ||
                    (reportParamDTO.getReportStatus() != null && "ALL".equals(reportParamDTO.getReportStatus())))) {
                page = reportRepository.findAll(pageable);
                return getPageInfo(pageInfo, page);
            }
            return pageInfo;
        } else {
            throw new PaginationSortingException(MessageConstants.INVALID_CONDITION);
        }
    }

    private PageInfo<?> getPageInfo(PageInfo<ReportDTO> pageInfo, Page<Report> page) {
        if (page.hasContent()) {
            pageInfo.setContent(page.getContent().stream().map(ReportDTO::new).collect(Collectors.toList()));
            pageInfo.setTotal(page.getTotalElements());
            pageInfo.setTotalPage(page.getTotalPages());
            pageInfo.setPageSize(page.getSize());
            pageInfo.setPage(page.getNumber());
        }
        return pageInfo;
    }

    private void createReport(String name, LocalDate dueDate, List<Criteria> criterias, Integer projectId, Integer employeeId, Integer reportToId) {
        Report report = new Report();
        report.setName(name);
        report.setProjectId(projectId);
        report.setDueDate(dueDate);
        report.setEmployeeId(employeeId);
        report.setStatus(EMEnum.ReportStatus.ONGOING);
        report.setReportToId(reportToId);
        report.setCreated(LocalDateTime.now());
        try {
            /*save Report*/
            report = reportRepository.save(report);
            /*save Report Detail*/
            Report finalReport = report;
            criterias.forEach(criteria -> {
                createReportDetails(finalReport, criteria);
            });

        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    private void updateReportOverdue(String reportName, Integer projectId, Integer employeeId, Integer reportToId, LocalDate dueDate) {
        try {
            Report reportUpdate = reportRepository.findByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus(reportName,
                    projectId,
                    employeeId,
                    reportToId,
                    EMEnum.ReportStatus.OVERDUE);
            reportUpdate.setDueDate(dueDate);
            reportUpdate.setStatus(EMEnum.ReportStatus.ONGOING);
            reportUpdate.setModified(LocalDateTime.now());
            reportRepository.save(reportUpdate);
        } catch (Exception e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    private void createReportDetails(Report report, Criteria criteria) {
        ReportDetail reportDetail = new ReportDetail();
        reportDetail.setCriteriaId(criteria.getId());
        reportDetail.setReportId(report.getId());
        reportDetail.setEvaluation((double) 0);
        reportDetail.setCreated(LocalDateTime.now());
        reportDetailRepository.save(reportDetail);
    }

    private ReportResponseDTO initReportLeader(List<Integer> employeeIds, List<Criteria> criterias, LocalDate dueDate) {
        if (!employeeIds.isEmpty()) {
            employeeIds.forEach(employeeId -> {
                /*List Projects EMPLOYEE joining*/
                List<Project> projects = projectRepository.findByEmployeeId(employeeId);
                if (!projects.isEmpty()) {
                    projects.forEach(project -> {
                        if (memberProjectRepository.existsByProjectIdAndEmployeeIdAndMemberTypeAndGetOutDate(project.getId(), employeeId, "MEMBER", null)) {
                            /*List Leaders in every Project*/
                            List<MemberProject> leaderProjects = memberProjectRepository
                                    .findAllByProjectIdAndMemberTypeAndGetOutDate(project.getId(), "LEADER", null);

                            if (!leaderProjects.isEmpty()) {
                                leaderProjects.forEach(leaderProject -> {
                                /*if(memberProjectRepository.existsByProjectIdAndEmployeeIdAndMemberType(project.getId(), employeeId, "MEMBER") &&
                                        !reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "LEADER EVALUATION",
                                                project.getId(),
                                                employeeId,
                                                leaderProject.getEmployeeId(),
                                                EMEnum.ReportStatus.ONGOING
                                        )){
                                    *//*If exist report with type OVERDUE*//*
                                    if(reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "LEADER EVALUATION",
                                            project.getId(),
                                            employeeId,
                                            leaderProject.getEmployeeId(),
                                            EMEnum.ReportStatus.OVERDUE)){
                                        updateReportOverdue("LEADER EVALUATION", project.getId(), employeeId, leaderProject.getEmployeeId(), dueDate);
                                    }
                                    *//*If not exist report with type COMPLETE*//*
                                    else if(!reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "LEADER EVALUATION",
                                            project.getId(),
                                            employeeId,
                                            leaderProject.getEmployeeId(),
                                            EMEnum.ReportStatus.COMPLETE
                                    )) {*/
                                    createReport("LEADER EVALUATION",
                                            dueDate,
                                            criterias,
                                            project.getId(),
                                            employeeId,
                                            leaderProject.getEmployeeId()
                                    );
                                   /* }
                                }*/
                                });
                            }
                        }

                    });
                }
            });
        }
        return new ReportResponseDTO(MessageConstants.REPORTS_INIT_SUCCESS);
    }

    private ReportResponseDTO initReportMember(List<Integer> employeeIds, List<Criteria> criterias, LocalDate dueDate) {
        if (!employeeIds.isEmpty()) {
            employeeIds.forEach(employeeId -> {
                /*check this Employee is Leader or PM*/
                Employee leaderOrPM = employeeRepository.findById(employeeId)
                        .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", employeeId));
                /*if this Employee is PM*/
                if (EMEnum.RoleName.ROLE_PM.equals(leaderOrPM.getUser().getRole().getName())) {
                    /*List Projects this PM joining*/
                    List<Project> projects = projectRepository.findByProjectManagerIdAndStatus(employeeId, EMEnum.ProjectStatus.ENABLE);
                    if (!projects.isEmpty()) {
                        projects.forEach(project -> {
                            List<MemberProject> memberProjects = memberProjectRepository
                                    .findAllByProjectIdAndMemberTypeAndGetOutDate(project.getId(), "MEMBER", null);

                            if (!memberProjects.isEmpty()) {
                                memberProjects.forEach(memberProject -> {
                                    /*If not exist report with type ONGOING*/
                                   /* if(!reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "MEMBER EVALUATION",
                                            memberProject.getProjectId(),
                                            employeeId,
                                            memberProject.getEmployeeId(),
                                            EMEnum.ReportStatus.ONGOING
                                    )) {
                                        *//*If exist report with type OVERDUE*//*
                                        if(reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "MEMBER EVALUATION",
                                                memberProject.getProjectId(),
                                                employeeId,
                                                memberProject.getEmployeeId(),
                                                EMEnum.ReportStatus.OVERDUE)){
                                            updateReportOverdue("MEMBER EVALUATION", project.getId(), employeeId, memberProject.getEmployeeId(), dueDate);
                                        }
                                        *//*If not exist report with type COMPLETE*//*
                                        else if(!reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "MEMBER EVALUATION",
                                                memberProject.getProjectId(),
                                                employeeId,
                                                memberProject.getEmployeeId(),
                                                EMEnum.ReportStatus.COMPLETE
                                        )) {*/
                                    createReport("MEMBER EVALUATION",
                                            dueDate,
                                            criterias,
                                            memberProject.getProjectId(),
                                            employeeId,
                                            memberProject.getEmployeeId()
                                    );
                                        /*}
                                    }*/
                                });
                            }

                        });
                    }
                } else if (EMEnum.RoleName.ROLE_LEADER.equals(leaderOrPM.getUser().getRole().getName())) {
                    /*List Project this Leader joining*/
                    List<MemberProject> projectLeaderJionings = memberProjectRepository
                            .findAllByEmployeeIdAndMemberTypeAndGetOutDate(employeeId, "LEADER", null);
                    if (!projectLeaderJionings.isEmpty()) {
                        projectLeaderJionings.forEach(project -> {
                            if (!projectRepository.existsByIdAndStatus(project.getProjectId(), EMEnum.ProjectStatus.DISABLE)) {
                                /*List Member need evaluation for each project*/
                                List<MemberProject> memberProjects = memberProjectRepository
                                        .findAllByProjectIdAndMemberTypeAndGetOutDate(project.getProjectId(), "MEMBER", null);

                                if (!memberProjects.isEmpty()) {
                                    memberProjects.forEach(memberProject -> {
                                        /*if(!reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "MEMBER EVALUATION",
                                                project.getProjectId(),
                                                employeeId,
                                                memberProject.getEmployeeId(),
                                                EMEnum.ReportStatus.ONGOING
                                        )) {
                                            *//*If exist report with type OVERDUE*//*
                                            if(reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "MEMBER EVALUATION",
                                                    project.getProjectId(),
                                                    employeeId,
                                                    memberProject.getEmployeeId(),
                                                    EMEnum.ReportStatus.OVERDUE)){
                                                updateReportOverdue("MEMBER EVALUATION", project.getProjectId(), employeeId, memberProject.getEmployeeId(), dueDate);
                                            }
                                            *//*If not exist report with type COMPLETE*//*
                                            else if(!reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "MEMBER EVALUATION",
                                                    project.getProjectId(),
                                                    employeeId,
                                                    memberProject.getEmployeeId(),
                                                    EMEnum.ReportStatus.COMPLETE
                                            )) {*/
                                        createReport("MEMBER EVALUATION",
                                                dueDate,
                                                criterias,
                                                project.getProjectId(),
                                                employeeId,
                                                memberProject.getEmployeeId());
                                            /*}
                                        }*/
                                    });
                                }
                            }
                        });
                    }
                }
            });
        }
        return new ReportResponseDTO(MessageConstants.REPORTS_INIT_SUCCESS);
    }

    private ReportResponseDTO initReportProject(List<Integer> employeeIds, List<Criteria> criterias, LocalDate dueDate) {
        if (!employeeIds.isEmpty()) {
            employeeIds.forEach(employeeId -> {
                Employee leaderOrPMOrMember = employeeRepository.findById(employeeId)
                        .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", employeeId));
                if (EMEnum.RoleName.ROLE_PM.equals(leaderOrPMOrMember.getUser().getRole().getName())) {
                    List<Project> projects = projectRepository.findByProjectManagerIdAndStatus(leaderOrPMOrMember.getId(), EMEnum.ProjectStatus.ENABLE);
                    if (!projects.isEmpty()) {
                        projects.forEach(project -> {
                           /* if(!reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "PROJECT EVALUATION",
                                    project.getId(),
                                    leaderOrPMOrMember.getId(),
                                    null,
                                    EMEnum.ReportStatus.ONGOING
                            )) {
                                *//*If exist report with type OVERDUE*//*
                                if(reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "PROJECT EVALUATION",
                                        project.getId(),
                                        leaderOrPMOrMember.getId(),
                                        null,
                                        EMEnum.ReportStatus.OVERDUE)){
                                    updateReportOverdue("PROJECT EVALUATION", project.getId(), leaderOrPMOrMember.getId(), null, dueDate);
                                }
                                *//*If not exist report with type COMPLETE*//*
                                else if(!reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "PROJECT EVALUATION",
                                        project.getId(),
                                        leaderOrPMOrMember.getId(),
                                        null,
                                        EMEnum.ReportStatus.COMPLETE
                                )) {*/
                            createReport("PROJECT EVALUATION",
                                    dueDate,
                                    criterias, project.getId(),
                                    leaderOrPMOrMember.getId(),
                                    null
                            );
                                /*}
                            }*/
                        });
                    }
                } else {
                    List<MemberProject> projectJionings = memberProjectRepository.findAllByEmployeeIdAndGetOutDate(leaderOrPMOrMember.getId(), null);
                    if (!projectJionings.isEmpty()) {
                        projectJionings.forEach(projectJioning -> {
                            if (!projectRepository.existsByIdAndStatus(projectJioning.getProjectId(), EMEnum.ProjectStatus.DISABLE)) {
                                /*if(!reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "PROJECT EVALUATION",
                                        projectJioning.getProjectId(),
                                        leaderOrPMOrMember.getId(),
                                        null,
                                        EMEnum.ReportStatus.ONGOING
                                )) {
                                    *//*If exist report with type OVERDUE*//*
                                    if(reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "PROJECT EVALUATION",
                                            projectJioning.getProjectId(),
                                            leaderOrPMOrMember.getId(),
                                            null,
                                            EMEnum.ReportStatus.OVERDUE)){
                                        updateReportOverdue("PROJECT EVALUATION", projectJioning.getProjectId(), leaderOrPMOrMember.getId(), null, dueDate);
                                    }
                                    *//*If not exist report with type COMPLETE*//*
                                    else if(!reportRepository.existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus( "PROJECT EVALUATION",
                                            projectJioning.getProjectId(),
                                            leaderOrPMOrMember.getId(),
                                            null,
                                            EMEnum.ReportStatus.COMPLETE
                                    )) {*/
                                createReport("PROJECT EVALUATION",
                                        dueDate,
                                        criterias,
                                        projectJioning.getProjectId(),
                                        leaderOrPMOrMember.getId(),
                                        null);
                                   /* }
                                }*/
                            }

                        });
                    }
                }
            });
        }
        return new ReportResponseDTO(MessageConstants.REPORTS_INIT_SUCCESS);
    }

    public PageInfo<?> getAllReportsByNotAdmin(ReportParamDTO reportParamDTO,
                                               Integer pageNo,
                                               Integer pageSize,
                                               String sortBy, Integer userId) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, Sort.by(sortBy));
        PageInfo<ReportDTO> pageInfo = new PageInfo<>();
        pageInfo.setContent(new ArrayList<>());
        Page<Report> page;
        Employee currentEmployee = employeeRepository.findByUserId(userId);
        if (EMEnum.ReportOrderBy.contains(sortBy)) {
            /*reportStatus is valid*/
            if (reportParamDTO != null && reportParamDTO.getReportStatus() != null &&
                    EMEnum.ReportStatus.contains(reportParamDTO.getReportStatus())) {
                page = reportRepository.getReportStatusByNotAdmin(currentEmployee.getId(),
                        pageable, EMEnum.ReportStatus.valueOf(reportParamDTO.getReportStatus()));
                return getPageInfo(pageInfo, page);

            } else if (reportParamDTO != null && (StringUtils.isBlank(reportParamDTO.getReportStatus()) ||
                    (reportParamDTO.getReportStatus() != null && "ALL".equals(reportParamDTO.getReportStatus())))) {
                page = reportRepository.getReportByNotAdmin(currentEmployee.getId(), pageable);
                return getPageInfo(pageInfo, page);
            }
            return pageInfo;
        } else {
            throw new PaginationSortingException(MessageConstants.INVALID_CONDITION);
        }
    }

    public PageInfo<?> getReport(ReportParamDTO reportParamDTO,
                                 Integer pageNo,
                                 Integer pageSize,
                                 String sortBy, Integer userId,
                                 String role) {
        if (role.equals("ROLE_ADMIN"))
            return getAllReportsByAdmin(reportParamDTO, pageNo, pageSize, sortBy);

        return getAllReportsByNotAdmin(reportParamDTO, pageNo, pageSize, sortBy, userId);
    }

    public PageInfo<?> getReportByEmId(Integer pageNo,
                                       Integer pageSize,
                                       String sortBy, Integer EmId, String role) {
        if (role.equals("ROLE_ADMIN")) {
            Pageable pageable = PageRequest.of(pageNo - 1, pageSize, Sort.by(sortBy));
            PageInfo<ReportDTO> pageInfo = new PageInfo<>();
            pageInfo.setContent(new ArrayList<>());
            Page<Report> page;
            if (EMEnum.ReportOrderBy.contains(sortBy)) {
                page = reportRepository.getReportByEmId(EmId, pageable);
                return getPageInfo(pageInfo, page);
            } else {
                throw new PaginationSortingException(MessageConstants.INVALID_CONDITION);
            }
        }
        throw new BadRequestException("UNAUTHORIZED");

    }


    public EvalutionDTO getReportDetails(Integer id) {
        Report report = reportRepository.getReportDetails(id);
        if (report != null) {
            EvalutionDTO evalutionDTO = new EvalutionDTO();
            BeanUtils.copyProperties(report, evalutionDTO);

            return evalutionDTO;
        }
        throw new ResourceNotFoundException("Report", "id", id);
    }

    public PageInfo<?> searchReports(ReportSearchParamDto searchParamDto,
                                     Integer pageNo,
                                     Integer pageSize,
                                     String sortBy,
                                     String role, Integer userId) {
        Pageable pageable = PageRequest.of(pageNo - 1, pageSize, Sort.by(sortBy));
        PageInfo<ReportDTO> pageInfo = new PageInfo<>();
        pageInfo.setContent(new ArrayList<>());
        Employee currentEmployee = employeeRepository.findByUserId(userId);
        Page<Report> page;
        if (EMEnum.ReportOrderBy.contains(sortBy)) {
            if (role.equals("ROLE_ADMIN")) {
                page = reportRepository.findReportCustomByAdmin(searchParamDto.getFullName(),
                        searchParamDto.getEmail(), searchParamDto.getProjectName(), pageable);
                return getPageInfo(pageInfo, page);
            } else {
                page = reportRepository.findReportCustomByNotAdmin(searchParamDto.getFullName(),
                        searchParamDto.getEmail(), searchParamDto.getProjectName(), currentEmployee.getId(), pageable);
                return getPageInfo(pageInfo, page);
            }
        } else {
            throw new PaginationSortingException(MessageConstants.INVALID_CONDITION);
        }
    }

    public List<ApiStatusDto> evaluationReport(List<EvaluationReportDTO> evaluationReportDTOs) {
        List<ApiStatusDto> apiStatusDtos = new ArrayList<>();
        for (EvaluationReportDTO evaluationReportDTO : evaluationReportDTOs) {
            Report report = reportRepository.findById(evaluationReportDTO.getReportId())
                    .orElseThrow(() -> new ResourceNotFoundException("Report", "id", evaluationReportDTO.getReportId()));

            List<ReportDetail> reportDetails = reportDetailRepository.findByReportId(evaluationReportDTO.getReportId());

            if (!evaluationReportDTO.getCriteriaDTOs().isEmpty() &&
                    !reportDetails.isEmpty() &&
                    evaluationReportDTO.getCriteriaDTOs().size() == reportDetails.size()) {

                long numberOfCriteriaEvaluated = reportDetails.stream()
                        .filter(reportDetail -> reportDetail.getCreated().equals(reportDetail.getModified()))
                        .count();

                if (LocalDate.now().isBefore(report.getDueDate())) {

                    setEvaluationAndCommentInReportDetail(evaluationReportDTO.getCriteriaDTOs(), reportDetails);

                    reportDetailRepository.saveAll(reportDetails);
                    report.setStatus(EMEnum.ReportStatus.COMPLETE);
                    reportRepository.save(report);
                    apiStatusDtos.add(new ApiStatusDto(true, evaluationReportDTO.getReportId().intValue()
                            + APIConstants.DASH
                            + MessageConstants.SUCCESS_EVALUATION));


                } else if (numberOfCriteriaEvaluated == reportDetails.size() &&
                        EMEnum.ReportStatus.OVERDUE.equals(report.getStatus())) {

                    setEvaluationAndCommentInReportDetail(evaluationReportDTO.getCriteriaDTOs(), reportDetails);

                    reportDetailRepository.saveAll(reportDetails);
                    apiStatusDtos.add(new ApiStatusDto(true, evaluationReportDTO.getReportId().intValue()
                            + APIConstants.DASH
                            + MessageConstants.SUCCESS_EVALUATION));
                }
            } else {
                apiStatusDtos.add(new ApiStatusDto(false, evaluationReportDTO.getReportId().intValue()
                        + APIConstants.DASH
                        + MessageConstants.FAIL_EVALUATION));
            }
        }
        return apiStatusDtos;
    }

    public void setEvaluationAndCommentInReportDetail(@Valid List<EvaluationScoresDTO> evaluationScoresDTO,
                                                      List<ReportDetail> reportDetails) throws BadRequestException {

        if (!reportDetails.isEmpty()) {
            reportDetails.forEach(reportDetail -> {
                Optional<EvaluationScoresDTO> evaluation = evaluationScoresDTO.stream()
                        .filter(criteria -> reportDetail.getId().equals(criteria.getCriteriaId()))
                        .findFirst();
                if (evaluation.isPresent()) {
                    if (!evaluation.get().getComment().isEmpty()) {
                        reportDetail.setComment(evaluation.get().getComment());
                    } else {
                        reportDetail.setComment(null);
                    }
                    if (evaluation.get().getEvaluation() >= 0 && evaluation.get().getEvaluation() <= 10) {
                        reportDetail.setEvaluation(evaluation.get().getEvaluation());
                    } else {
                        throw new BadRequestException(MessageConstants.VALIDATION_SCORES_EVALUATION);
                    }
                }
                evaluationScoresDTO.remove(evaluation.get());
            });

        }
    }
}
