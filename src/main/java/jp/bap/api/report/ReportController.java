package jp.bap.api.report;

import jp.bap.api.project.dto.ErrorResponseDTO;
import jp.bap.api.report.dto.EvaluationReportDTO;
import jp.bap.api.report.dto.ReportParamDTO;
import jp.bap.api.report.dto.ReportRequestDTO;
import jp.bap.common.exception.BadRequestException;
import jp.bap.common.exception.ResourceNotFoundException;
import jp.bap.api.report.dto.ReportSearchParamDto;
import jp.bap.common.response.APIResponse;
import jp.bap.common.util.SecurityUtil;
import jp.bap.common.util.constant.APIConstants;
import jp.bap.common.util.constant.MessageConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ReportController {

    @Autowired
    private ReportService reportService;


    @GetMapping("/reports")
    public APIResponse<?> getAllReports(@Valid @RequestParam (name = "filter", required = false, defaultValue = "ALL") String reportParamDTO,
                                               @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNO,
                                               @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize,
                                               @RequestParam(name = "sort", required = false, defaultValue = "id") String sortBy){

//        APIResponse<?> fieldErrors = getErrorResponse(bindingResult);
//        if (fieldErrors != null) return fieldErrors;
        ReportParamDTO reportParam = new ReportParamDTO();
        reportParam.setReportStatus(reportParamDTO);
        return APIResponse.okStatus(reportService.getReport(reportParam,
                pageNO,
                pageSize,
                sortBy,
                SecurityUtil.getUser().getId(),
                SecurityUtil.getRoleName().toString()),
                APIConstants.SUCCESS_CODE);
    }

    @GetMapping("/reports/{EmId}")
    public APIResponse<?> getReportByEmId(@Valid
                                        @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNO,
                                        @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize,
                                        @RequestParam(name = "sort", required = false, defaultValue = "id") String sortBy,
                                        @PathVariable("EmId") Integer EmId){
        return APIResponse.okStatus(reportService.getReportByEmId(pageNO,
                pageSize,
                sortBy,
                EmId,
                SecurityUtil.getRoleName().toString()),
                APIConstants.SUCCESS_CODE);
    }

    @PostMapping("reports/search")
    public APIResponse<?> searchReports(@Valid @RequestBody ReportSearchParamDto searchParamDto,
                                        @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNO,
                                        @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize,
                                        @RequestParam(name = "sort", required = false, defaultValue = "id") String sortBy){
        return APIResponse.okStatus(reportService.searchReports(searchParamDto,
                pageNO,
                pageSize,
                sortBy,
                SecurityUtil.getRoleName().toString(),
                SecurityUtil.getUser().getId()));
    }

    @GetMapping("/reportdetails/{id}")
    public APIResponse<?> getReportDetails(@Valid
                                        @PathVariable("id") Integer id){
        return APIResponse.okStatus(reportService.getReportDetails(id),
                APIConstants.SUCCESS_CODE);
    }

    /**
     *
     * @param reportRequestDTO {@link ReportRequestDTO}
     * @param bindingResult    ai
     * @return jp.bap.api.report.dto.ReportResponseDTO
     */
    @PostMapping ("/reports/admin")
    public APIResponse<?> createReportRequest(@Valid @RequestBody ReportRequestDTO reportRequestDTO,
                                              BindingResult bindingResult) {
        APIResponse<?> fieldErrors = getErrorResponse(bindingResult);
        if (fieldErrors != null) return fieldErrors;
        return APIResponse.okStatus(reportService.createReportRequest(reportRequestDTO), APIConstants.CREATE_CODE);
    }

    /**
     * Evaluation Report
     *
     * @param evaluationReportDTOs  {@link EvaluationReportDTO}
     * @return {@link APIResponse} data is {@link Object}
     * @throws ResourceNotFoundException
     * @throws BadRequestException
     */
    @PutMapping("/reports")
    public APIResponse<?> evaluationReport(@Valid @RequestBody List<EvaluationReportDTO> evaluationReportDTOs)
            throws ResourceNotFoundException, BadRequestException {
        return APIResponse.okStatus(reportService.evaluationReport(evaluationReportDTOs));
    }

    /**
     * @param bindingResult ai
     * @return list error
     */
    private APIResponse<?> getErrorResponse(BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            List<ErrorResponseDTO> errorResponseDTOS = new ArrayList<>();
            for(FieldError fieldError : fieldErrors){
                ErrorResponseDTO errorResponseDTO = new ErrorResponseDTO(fieldError.getField(),
                        fieldError.getDefaultMessage());
                errorResponseDTOS.add(errorResponseDTO);
            }

            return APIResponse.errorStatus(APIConstants.BAD_REQUEST_CODE, errorResponseDTOS, HttpStatus.BAD_REQUEST);
        }
        return null;
    }
}
