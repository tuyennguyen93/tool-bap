package jp.bap.api.employee.dto;

import jp.bap.common.model.User;
import lombok.Getter;
import lombok.Setter;

public class EmployeeByRoleNameDTO {

    @Getter @Setter
    private Integer id;

    @Getter @Setter
    private String fullName;

    @Setter
    private User user;

    public String getEmail() {
        return user.getEmail();
    }
}
