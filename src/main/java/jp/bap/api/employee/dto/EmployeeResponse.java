package jp.bap.api.employee.dto;

import io.swagger.models.auth.In;
import jp.bap.api.project.dto.ProjectDetailsResponseDTO;
import jp.bap.common.EMEnum;
import jp.bap.common.model.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeResponse {
    @Getter @Setter
    private Integer id;
    @Getter @Setter
    private String fullName;
    @Setter
    private User user;
    @Getter @Setter
    private LocalDate dob;

    private List<MemberProject> joinProjects = new ArrayList<>();

    private List<Project> managerProjects = new ArrayList<>();

    @Getter @Setter
    private LocalDate officialDate;

    @Getter @Setter
    private List<String> technology = new ArrayList<>();

    @Getter @Setter
    private Double salary;

    @Getter @Setter
    private LocalDate joinDate;



    public String getEmail() {
        return user.getEmail();
    }
    public boolean isActive() {
        return user.isActive();
    }


    public List<String> getProjectName(){
        List<String> project = new ArrayList<>();
        if(user.getRole().getName().toString().equals("ROLE_PM")) {
            project = managerProjects.stream().map(e -> e.getName()).collect(Collectors.toList());
        }
        else {
            project = joinProjects.stream().map(e-> e.getProject().getName()).collect(Collectors.toList());
        }
        return project;
    }

    public EMEnum.RoleName getRoleName(){
        return user.getRole().getName();
    }


    public void setJoinProjects(List<MemberProject> joinProjects) {
        joinProjects.forEach(memberProject -> {
                    if(memberProject.getGetOutDate()==null) {
                        this.joinProjects.add(memberProject);
                    }
                }
        );
    }


    public void setManagerProjects(List<Project> managerProjects) {
        this.managerProjects = managerProjects;
    }


}
