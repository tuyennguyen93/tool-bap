package jp.bap.api.employee.dto;

import jp.bap.common.model.Employee;
import jp.bap.common.model.Project;
import lombok.Data;
import org.springframework.beans.BeanUtils;

import java.time.LocalDate;
@Data
public class EmployeeCreateResponse {
    private int id;
    private String fullName;
    private String email;
    private String password;
    private LocalDate dob;
    private String phonenumber;
    private Integer roleId;
}
