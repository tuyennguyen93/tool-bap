package jp.bap.api.employee.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class TechnologyDTO {
    private Integer id;

    private String name;

    private LocalDateTime created;

}
