package jp.bap.api.employee.dto;

import lombok.Data;

@Data
public class EmployeeSearchParam {
    private String fullName;
    private String projectName;
    private String email;
}
