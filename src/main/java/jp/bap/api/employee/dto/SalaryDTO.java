package jp.bap.api.employee.dto;

import jp.bap.common.dto.BaseDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class SalaryDTO {
    private Integer id;

    private Double salary;

    private LocalDateTime created;
}
