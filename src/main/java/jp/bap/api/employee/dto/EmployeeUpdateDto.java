package jp.bap.api.employee.dto;

import lombok.Data;

import java.time.LocalDate;
@Data
public class EmployeeUpdateDto {
    private String fullName;
    private LocalDate dob;
    private LocalDate joinDate;
    private LocalDate officialDate;
    private Double salary;

}
