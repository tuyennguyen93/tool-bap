package jp.bap.api.employee;

import jp.bap.api.employee.dto.*;
import jp.bap.api.employee.mapper.EmployeeMapper;
import jp.bap.common.EMEnum;
import jp.bap.common.base.BaseService;
import jp.bap.common.dto.PageInfo;
import jp.bap.common.exception.BadRequestException;
import jp.bap.common.exception.PaginationSortingException;
import jp.bap.common.exception.ResourceNotFoundException;
import jp.bap.common.model.Employee;
import jp.bap.common.model.Salary;
import jp.bap.common.model.Technology;
import jp.bap.common.model.User;
import jp.bap.common.repository.EmployeeRepository;
import jp.bap.common.repository.SalaryRepository;
import jp.bap.common.repository.TechnologyRepository;
import jp.bap.common.repository.UserRepository;
import jp.bap.common.util.constant.MessageConstants;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmployeeService extends BaseService {
    @Autowired
    EmployeeRepository repository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    EmployeeMapper employeeMapper;

    @Autowired
    TechnologyRepository technologyRepository;

    @Autowired
    SalaryRepository salaryRepository;


    public PageInfo<EmployeeResponse> getAllEmployee(Integer userId, String role,
                                                     Integer pageNo, Integer pageSize) {
        if(pageNo >0){
            if(repository.isExistEmployee(userId)==null)
                throw new ResourceNotFoundException("Employee","userId",userId);
            Employee currentEmployee = repository.findByUserId(userId);
            int pageNoInDatabase = pageNo - 1;
            Pageable pageable = PageRequest.of(pageNoInDatabase, pageSize);
            Page<Employee> employees;
            PageInfo<EmployeeResponse> employeeResponses =  new PageInfo<EmployeeResponse>();
            employeeResponses.setContent(new ArrayList<>());
            switch(role)
            {
                case "ROLE_ADMIN" : {
                    employees = repository.findAll(pageable);
                    break;
                }
                case "ROLE_PM" : {
                    employees = repository.getEmployeeByPM(currentEmployee.getId(),pageable);
                    break;
                }
                case ("ROLE_LEADER"):
                case ("ROLE_MEMBER"):
                case ("ROLE_QCQA"): {
                    employees = repository.getEmployeeByMember(currentEmployee.getId(),pageable);
                    break;
                }
                default:
                    return employeeResponses ;
            }
            if (employees.hasContent()) {
                employeeResponses = employeeMapper.convertPageInfo(employees);
                return employeeResponses;
            } else
                return employeeResponses;
        }else {
            throw new PaginationSortingException(MessageConstants.INVALID_CONDITION);
        }

    }

    public EmployeeResponse getEmployeeById(Integer id, Integer userId, String role) throws ResourceNotFoundException {
        if(repository.isExistEmployee(userId)==null)
            throw new ResourceNotFoundException("Employee","userId",userId);
        if(role.equals("ROLE_ADMIN")) {
            Optional<Employee> employee = repository.findById(id);
            employee.get().getUser();
            if(employee.isPresent()) {
                return employeeMapper.getEmployDtofromModel(employee.get());
            } else {
                throw new ResourceNotFoundException("Employee", "id", id);
            }
        }
        else if(role.equals("ROLE_PM"))
        {
            Optional<Employee> employee = repository.getEmployeeOfPMById(userId,id);
            employee.get().getUser();
            if(employee.isPresent()) {
                return employeeMapper.getEmployDtofromModel(employee.get());
            } else {
                throw new ResourceNotFoundException("Employee", "id", id);
            }
        }
        throw new BadRequestException("UNAUTHORIZED");
    }

    public List<EmployeeByRoleNameDTO> getEmployeeByRoleName(EMEnum.RoleName roleName) {
        List<Employee> employees = repository.findEmployeeByRoleName(roleName);
        return employees.stream().map(employee -> {
            return employeeMapper.getEmployByRoleNameDTOFromModel(employee);
        }).collect(Collectors.toList());
    }

    public PageInfo<EmployeeResponse> getAllEmployeeByProject(Integer id, Integer userId, String role,
                                                              Integer pageNo, Integer pageSize) {
        if(repository.isExistEmployee(userId)==null)
            throw new ResourceNotFoundException("Employee","userId",userId);
        if(pageNo > 0){
            int pageNoInDatabase = pageNo - 1;
            Pageable pageable = PageRequest.of(pageNoInDatabase, pageSize);
            PageInfo<EmployeeResponse> employeeResponses =  new PageInfo<>();
            employeeResponses.setContent(new ArrayList<>());
            Page<Employee> employees;
            if(role.equals("ROLE_ADMIN")) {
                employees = repository.getEmployeeByProjectForAdmin(id, pageable);
            }
            else if(role.equals("ROLE_PM")) {
                employees=repository.getEmployeeByProjectForPM(id,userId,pageable);
            }
            else throw new BadRequestException("UNAUTHORIZED"); ;
            if (employees.hasContent()) {
                employeeResponses = employeeMapper.convertPageInfo(employees);
                return employeeResponses;
            } else
                return employeeResponses;
        }else {
            throw new PaginationSortingException(MessageConstants.INVALID_CONDITION);
        }
    }


    public List<EmployeeResponse> isActice(String role, Integer... ids) throws ResourceNotFoundException
    {
        if(role.equals("ROLE_ADMIN")) {
            List<EmployeeResponse> employeeResponses = new ArrayList<>();
            for (Integer id : ids) {
                Employee employee = repository.findById(id)
                        .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", id));
                if (employee.getUser().isActive() == true)
                    employee.getUser().setActive(false);
                else employee.getUser().setActive(true);
                Employee update = repository.save(employee);
                employeeResponses.add(employeeMapper.getEmployDtofromModel(employee));
            }
            return employeeResponses;
        }
        throw new BadRequestException("UNAUTHORIZED");
    }


    public void deleteEmployee(Integer id) throws ResourceNotFoundException
    {
        Employee employee = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", id));
        repository.delete(employee);
    }


    public EmployeeCreateResponse createEmployee(EmployeeRequest employeeRequest, String role){
        if(role.equals("ROLE_ADMIN")) {
            Optional<User> userList = userRepository.findByEmail(employeeRequest.getEmail());
            if (userList.isPresent()) {
                throw new BadRequestException(MessageConstants.EMAIL_ALREADY_EXISTS);
            }
            LocalDate timeNow = LocalDate.now();
            Employee employee = new Employee();
            employee.setFullName(employeeRequest.getFullName());
            employee.setDob(employeeRequest.getDob());
            employee.setJoinDate(timeNow);
            employee.setPhoneNumber(employeeRequest.getPhonenumber());
            User user = new User();
            user.setRoleId(employeeRequest.getRoleId());
            user.setEmail(employeeRequest.getEmail());
            user.setPassword(passwordEncoder.encode(employeeRequest.getPassword()));
            Employee resultEmployee = repository.save(employee);
            user.setEmployeeId(employee.getId());
            user.setEmployee(employee);
            User resulttUser = userRepository.save(user);
            EmployeeCreateResponse employeeCreateResponse = new EmployeeCreateResponse();
            employeeCreateResponse.setId(employee.getId());
            BeanUtils.copyProperties(employeeRequest,employeeCreateResponse);
            return employeeCreateResponse;
        }
        throw new BadRequestException("UNAUTHORIZED");
    }

    public ApiResponseDto updateEmployee(Integer id, EmployeeUpdateDto employeeUpdateDto, String role){
        if(role.equals("ROLE_ADMIN")) {
            Employee employee = repository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", id));
            employee.setFullName(employeeUpdateDto.getFullName());
            employee.setDob(employeeUpdateDto.getDob());
            employee.setJoinDate(employeeUpdateDto.getJoinDate());
            employee.setOfficialDate(employeeUpdateDto.getOfficialDate());
         if(employeeUpdateDto.getSalary() != null) {
             Salary salary = new Salary();
             employee.setSalaryPresent(employeeUpdateDto.getSalary());
             salary.setEmployee(employee);
             salary.setSalary(employeeUpdateDto.getSalary());
             salaryRepository.save(salary);
         } else {
             Employee update = repository.save(employee);
         }

            return new ApiResponseDto(true, MessageConstants.UPDATE_EMPLOYEE_SUCCES);
        }
        throw new BadRequestException("UNAUTHORIZED");
    }


    public PageInfo<EmployeeResponse> searchEmployee(String fullName, String projectName, String email, String  type, String roleSearch, Integer salaryFrom, Integer salaryTo ,
                                                     Integer pageNo, Integer pageSize, String sortBy, String... technology){
        Integer id = this.getCurrentUser().getId();
        String role = this.getCurrentRoleName().toString();

        if(pageNo > 0){
            Pageable pageable = this.buildPageRequest(pageNo,pageSize, Sort.by(sortBy));
            Page<Employee> employees;
            PageInfo<EmployeeResponse> employeeResponses =  new PageInfo<>();
            employeeResponses.setContent(new ArrayList<>());
            switch(role)
            {
                case "ROLE_ADMIN" : {
                    if (technology != null) {
                        employees = repository.findAllCustomerByAdmin(fullName, projectName, email, type, roleSearch, salaryFrom, salaryTo, pageable, technology);
                    } else {
                        employees = repository.findAllCustomerByAdminNoTech(fullName, projectName, email, type, roleSearch, salaryFrom, salaryTo, pageable);
                    }
                    break;
                }
                case "ROLE_PM" : {
                    if (technology != null) {
                        employees = repository.findProjectManager(fullName, projectName, email, id, type, roleSearch, salaryFrom, salaryTo, pageable, technology);
                    } else {
                        employees = repository.findProjectManagerNoTech(fullName, projectName, email, id, type, roleSearch, salaryFrom, salaryTo, pageable);
                    }
                    break;
                }
                default:
                    return employeeResponses;
            }
            if(!employees.isEmpty()) {
                employeeResponses = employeeMapper.convertPageInfo(employees);
                return employeeResponses;
            }
            else
                return employeeResponses;
        }else {
            throw new PaginationSortingException(MessageConstants.INVALID_CONDITION);
        }
    }


    public List<String> getAllTechnology() {
        List<String> lst = new ArrayList<>();
        List<Technology> technologyList = technologyRepository.findAll();
        lst = technologyList.stream().map(e -> e.getName()).distinct().collect(Collectors.toList());
        return lst;
    }

    public List<TechnologyDTO> getAllTechnologyByEmployId(Integer id) {
        List<TechnologyDTO> lst = new ArrayList<>();
        List<Technology> technologyList = technologyRepository.findAllByIdEmploye(id);
        lst = employeeMapper.convertToListTechnologyDto(technologyList);
        return lst;
    }

    public void deleteTechnology(Integer id) {
        List<String> lst = new ArrayList<>();
        technologyRepository.deleteById(id);
    }



}
