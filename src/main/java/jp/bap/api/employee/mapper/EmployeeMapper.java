package jp.bap.api.employee.mapper;

import jp.bap.api.employee.dto.EmployeeByRoleNameDTO;
import jp.bap.api.employee.dto.EmployeeResponse;
import jp.bap.api.employee.dto.TechnologyDTO;
import jp.bap.common.dto.PageInfo;
import jp.bap.common.model.Employee;
import jp.bap.common.model.Salary;
import jp.bap.common.model.Technology;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tuyennv
 */
@Service
@RequiredArgsConstructor

public class EmployeeMapper {
    public EmployeeResponse getEmployDtofromModel(Employee employee){
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee, employeeResponse);

        employeeResponse.setSalary(employee.getSalaryPresent());

        List <String> technologyName = new ArrayList<>();
        employee.getTechnology().forEach( i -> {
            technologyName.add(i.getName());
        });
        employeeResponse.setTechnology(technologyName);

        return employeeResponse;
    }

    public EmployeeByRoleNameDTO getEmployByRoleNameDTOFromModel(Employee employee){
        EmployeeByRoleNameDTO employeeByRoleNameDTO = new EmployeeByRoleNameDTO();
        BeanUtils.copyProperties(employee, employeeByRoleNameDTO);
        return employeeByRoleNameDTO;
    }

    public PageInfo<EmployeeResponse> convertPageInfo(Page<Employee> employees){
        PageInfo<EmployeeResponse> employeeResponses =  new PageInfo<>();
        employeeResponses.setTotal(employees.getTotalElements());
        employeeResponses.setPageSize(employees.getSize());
        employeeResponses.setTotalPage(employees.getTotalPages());
        employeeResponses.setPage(employees.getNumber());
        employeeResponses.setContent(employees.getContent().stream()
                .map(e->getEmployDtofromModel(e))
                .collect(Collectors.toList()));
        return employeeResponses;
    }

    public Double salaryPresent(List<Salary> salaries) {
        Double salary = null;
        if(salaries.size() >0) {
            LocalDateTime created = salaries.get(0).getCreated();
            salary = salaries.get(0).getSalary();
            for (Salary i : salaries) {
                if (i.getCreated().compareTo(created) > 0) {
                    salary = i.getSalary();
                }
            }
        }
        return salary;
    }

    public List<TechnologyDTO> convertToListTechnologyDto(List<Technology> technologyList) {
     List<TechnologyDTO> technologyDtos = new ArrayList<>();
     technologyList.forEach(i -> {
         technologyDtos.add(convertToTechnologyDto(i));
     });
     return technologyDtos;
    }

    public TechnologyDTO convertToTechnologyDto(Technology technology) {
        TechnologyDTO technologyDTO = new TechnologyDTO();
        technologyDTO.setId(technology.getId());
        technologyDTO.setName(technology.getName());
        technologyDTO.setCreated(technology.getCreated());
        return technologyDTO;
    }

}
