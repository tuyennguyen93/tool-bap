package jp.bap.api.employee;

import io.swagger.annotations.ApiParam;
import jp.bap.api.employee.dto.*;
import jp.bap.common.EMEnum;
import jp.bap.common.exception.BadRequestException;
import jp.bap.common.exception.ResourceNotFoundException;


import jp.bap.common.model.Employee;
import jp.bap.common.repository.EmployeeRepository;
import jp.bap.common.repository.UserRepository;
import jp.bap.common.response.APIResponse;
import jp.bap.common.util.SecurityUtil;
import jp.bap.common.util.constant.APIConstants;
import jp.bap.common.util.constant.MessageConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;



@RestController
@RequestMapping("/api/v1")
public class EmployeeController {
    @Autowired
    EmployeeService service;
    @Autowired
    UserRepository userRepository;

    @Autowired
    EmployeeRepository employeeRepository;


    @GetMapping("/employees")
    public APIResponse<?> getAllEmployee(@RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNO,
                                         @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize){
        if(SecurityUtil.getUser() !=null){
            return APIResponse.okStatus(
                    service.getAllEmployee(SecurityUtil.getUser().getId()
                            ,SecurityUtil.getRoleName().toString(), pageNO, pageSize)
            );
        }
        return APIResponse.errorStatus(APIConstants.UNAUTHORIZED_CODE, MessageConstants.NOT_AUTHENTICATED, HttpStatus.UNAUTHORIZED);
    }


    @GetMapping("/employees/{id}")
    public APIResponse<?> getAllEmployee(@PathVariable("id") Integer id)
            throws ResourceNotFoundException {
        return APIResponse.okStatus(service.getEmployeeById(id ,
                SecurityUtil.getUser().getId() ,SecurityUtil.getRoleName().toString()));
    }

    @GetMapping("/employees/roles")
    public APIResponse<?> getEmployeeByRoleName(@RequestParam EMEnum.RoleName roleName)
            throws BadRequestException {
        if (EMEnum.RoleName.ROLE_PM.equals(roleName) || EMEnum.RoleName.ROLE_LEADER.equals(roleName)) {
            return APIResponse.okStatus(service.getEmployeeByRoleName(roleName));
        } else {
            throw new BadRequestException(MessageConstants.NOT_GET_EMPLOYEES_BY_ROLE_NAME);
        }
    }

    @GetMapping("/employees/project/{id}")
    public APIResponse<?> getAllEmployeeByProject(@PathVariable("id") Integer id,
                                                  @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNO,
                                                  @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize)
            throws ResourceNotFoundException {
        return APIResponse.okStatus(service.getAllEmployeeByProject(id,SecurityUtil.getUser().getId(),
                SecurityUtil.getRoleName().toString(), pageNO, pageSize));
    }


    @PostMapping("/employees")
    public APIResponse<?> createEmployee(@Valid @RequestBody EmployeeRequest employeeRequest)
            throws ResourceNotFoundException {
        return APIResponse.okStatus(service.createEmployee(employeeRequest ,SecurityUtil.getRoleName().toString()),
                APIConstants.SUCCESS_CODE);
    }


    @PutMapping("employees/{id}")
    public APIResponse<ApiResponseDto> updateEmployeeById(@PathVariable(value = "id") int id,
                                                          @Valid @RequestBody EmployeeUpdateDto employeeupdate){
        return APIResponse.okStatus(service.updateEmployee(id ,employeeupdate,SecurityUtil.getRoleName().toString()));
    }


    @PutMapping("/employees/active")
    public APIResponse<?> isActive(@RequestParam Integer ... ids)
            throws ResourceNotFoundException {
        return APIResponse.okStatus(service.isActice(SecurityUtil.getRoleName().toString() ,ids));
    }


    @GetMapping("/employees/search")
    public APIResponse<?> fullTextSearch(@RequestParam(name = "fullName", required = false, defaultValue = "") String fullName,
                                         @RequestParam(name = "projectName", required = false, defaultValue = "") String projectName,
                                         @RequestParam(name = "StaffType", required = false, defaultValue = "") EMEnum.StaffType staffType,
                                         @RequestParam(name = "roleSearch", required = false, defaultValue = "") EMEnum.RoleName roleSearch,
                                         @RequestParam(name = "salaryFrom", required = false, defaultValue = "") Integer salaryFrom,
                                         @RequestParam(name = "salaryTo", required = false, defaultValue = "") Integer salaryTo,
                                         @RequestParam(name = "email", required = false, defaultValue = "") String email,
                                         @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNO,
                                         @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize,
                                         @ApiParam(value = "Sort By", required = false, allowableValues = "id, fullName, joinDate,officialDate, salaryPresent")
                                         @RequestParam(name = "sort", required = false, defaultValue = "id") String sort,
                                         @RequestParam(name = "nameTechs", required = false)  String... nameTechs
    ) {
        if(SecurityUtil.getUser() !=null){
            salaryFrom = salaryFrom == null?-1:salaryFrom;
            salaryTo = salaryTo == null?-1:salaryTo;
            String staffTypecode = staffType == null?"":staffType.getCode();
            String roleSearchCode = roleSearch == null?"":roleSearch.getCode();

            return APIResponse.okStatus(
                    service.searchEmployee(fullName, projectName, email, staffTypecode, roleSearchCode, salaryFrom, salaryTo,
                            pageNO, pageSize, sort, nameTechs )
            );
        }
        else return APIResponse.errorStatus(APIConstants.UNAUTHORIZED_CODE, MessageConstants.NOT_AUTHENTICATED, HttpStatus.UNAUTHORIZED);
    }

    @GetMapping("/technologies")
    public APIResponse<?> getAllTechnology() {
        return APIResponse.okStatus(service.getAllTechnology());
    }

    @GetMapping("/technologies/employees/{id}")
    public APIResponse<?> getAllTechnologyByEmployId(@PathVariable(value = "id") int id) {
        return APIResponse.okStatus(service.getAllTechnologyByEmployId(id));
    }

    @DeleteMapping("/technologies/{id}")
    public APIResponse<?> deleteTechnology(@PathVariable(value = "id") int id) {
        service.deleteTechnology(id);
        return APIResponse.okStatus( APIConstants.SUCCESS_CODE);
    }



/*    @DeleteMapping("/employee/{id}")
    public HttpStatus deteleEmployee(@PathVariable("id") Integer id)
            throws ResourceNotFoundException {
        service.deleteEmployee(id);
        return HttpStatus.FORBIDDEN;
    }*/

}
