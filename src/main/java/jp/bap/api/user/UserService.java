package jp.bap.api.user;

import jp.bap.common.EMEnum;
import jp.bap.common.dto.*;
import jp.bap.common.exception.BadRequestException;
import jp.bap.common.exception.PaginationSortingException;
import jp.bap.common.exception.ResourceNotFoundException;
import jp.bap.common.model.*;
import jp.bap.common.repository.EmployeeRepository;
import jp.bap.common.repository.RolePermissionRepository;
import jp.bap.common.repository.RoleRepository;
import jp.bap.common.repository.UserRepository;
import jp.bap.common.util.ExcelUtils;
import jp.bap.common.util.ModelMapper;
import jp.bap.common.util.UserUtils;
import jp.bap.common.util.constant.APIConstants;
import jp.bap.common.util.constant.MessageConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContextException;
import org.springframework.core.io.InputStreamResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;

import org.springframework.data.domain.Pageable;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserUtils userUtil;
    @Autowired
    RolePermissionRepository rolePermissionRepository;

    public PageInfo<UserDTO> getAllUsers(Integer pageNo, Integer pageSize) throws
            BadRequestException, PaginationSortingException {
        if (pageNo > 0) {

            int pageNoInDatabase = pageNo - 1;

            Pageable pageable = PageRequest.of(pageNoInDatabase, pageSize);
            Page<User> pagedResult = userRepository.findAll(pageable);
            PageInfo<UserDTO> pageInfo = new PageInfo<>();

            if (pagedResult.hasContent()) {

                pageInfo.setTotal(pagedResult.getTotalElements());
                pageInfo.setPageSize(pagedResult.getSize());
                pageInfo.setTotalPage(pagedResult.getTotalPages());
                pageInfo.setPage(pagedResult.getNumber());
                pageInfo.setContent(pagedResult.getContent()
                        .stream()
                        .map(UserDTO::new)
                        .collect(Collectors.toList()));

                return pageInfo;

            } else {
                return pageInfo;
            }
        } else {
            throw new PaginationSortingException(MessageConstants.INVALID_CONDITION);
        }
    }

    public UserDTO getUserById(Integer id) throws ResourceNotFoundException {
        return new UserDTO(userRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", id)));
    }

    public List<UserDTO> isActiveUser(Integer... ids) throws ResourceNotFoundException {
        List<UserDTO> userDTOs = new ArrayList<>();
        for (Integer id : ids) {
            userDTOs.add(new UserDTO(userRepository.findById(id).map(user -> {
                user.setActive(!user.isActive());
                return userRepository.save(user);
            }).orElseThrow(() -> new ResourceNotFoundException("User", "id", id))));
        }
        return userDTOs;
    }

    public UserProfileDto convertUserData(UserPrincipalDto currentUser) {
        User user = userRepository.findById(currentUser.getId()).orElseThrow(
                () -> new ResourceNotFoundException("User", "id", currentUser.getId())
        );
        Role role = (user.getRoleId() != null) ? user.getRole() : null;
        Employee employee = user.getEmployee();
        Level level = (user.getEmployee() != null && user.getEmployee().getLevel() != null)
                ? user.getEmployee().getLevel() : null;
        return ModelMapper.mapToUserProfile(role, level, employee, user);
    }

    public ApiStatusDto saveUser(@Valid SignUpRequestDto signUpRequest) throws BadRequestException {
        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            throw new BadRequestException(MessageConstants.EMAIL_ALREADY_EXISTS);
        }
        LocalDate timeJoin = LocalDate.now();
        User user = new User();
        user.setEmail(signUpRequest.getEmail());
        user.setPassword(signUpRequest.getPassword());
        Employee employee = new Employee();
        employee.setDob(signUpRequest.getDob());
        employee.setFullName(signUpRequest.getFullName());
        employee.setPhoneNumber(signUpRequest.getPhoneNumber());
        employee.setJoinDate(timeJoin);
        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
        Role userRole = roleRepository.findByName(EMEnum.RoleName.ROLE_MEMBER)
                .orElseThrow(() -> new ApplicationContextException(MessageConstants.ROLE_ALREADY_EXISTS));
        user.setRoleId(userRole.getId());
        Employee resultEmployee = employeeRepository.save(employee);
        user.setEmployeeId(resultEmployee.getId());
        User resultUser = userRepository.save(user);
        return new ApiStatusDto(true, MessageConstants.USER_REGISTER_SUCCESS);
    }

    public ApiStatusDto changePasswordUser(ChangePasswordParam param, UserPrincipalDto currentUser) {
        if (!currentUser.getEmail().equals(param.getEmail())) {
            throw new ResourceNotFoundException("User", "email", param.getEmail());
        }
        User user = userRepository.findByEmail(param.getEmail()).orElseThrow(
                () -> new ResourceNotFoundException("User", "email", param.getEmail())
        );
        if (!passwordEncoder.matches(param.getOldPassword(), user.getPassword())) {
            throw new BadRequestException(MessageConstants.INVALID_PASSWORD);
        }
        user.setPassword(passwordEncoder.encode(param.getNewPassword()));
        userRepository.save(user);
        return new ApiStatusDto(true, MessageConstants.CHANGE_PASSWORD_SUCCESS);
    }

    public UserImportDto importUser(MultipartFile file) throws IOException {
        List<SignUpRequestDto> data = ExcelUtils.readContent(file);
        List<Object> errorList = new ArrayList<>();
        List<SignUpRequestDto> successList = new ArrayList<>();
        data.forEach(signUpRequestDto -> {
            ErrorDto usr = userUtil.validationUser(signUpRequestDto);
            if (usr.getEmail() != null && !usr.getErrors().isEmpty()) {
                errorList.add(usr);
            } else {
                successList.add(signUpRequestDto);
                saveUser(signUpRequestDto);
            }
        });
        return new UserImportDto(successList, errorList, data.size());
    }

    public InputStreamResource dowloadTemp(String fileName) throws IOException {
        return ExcelUtils.getfileXLSX(fileName);
    }

    public ApiStatusDto resetPasswordUserByAdmin(Integer userId, String newPassword) {

        User user = userRepository.findById(userId).orElseThrow(
                () -> new ResourceNotFoundException("User", "id", userId)
        );

        if (newPassword.isEmpty()) {
            return new ApiStatusDto(false, MessageConstants.PASSW_NULL);
        } else if (!newPassword.isEmpty() && newPassword.length() < 6) {
            return new ApiStatusDto(false, MessageConstants.PASSW_LENGTH);
        } else {
            userRepository.updatePassword(passwordEncoder.encode(newPassword), userId);
            return new ApiStatusDto(true, MessageConstants.CHANGE_PASSWORD_SUCCESS);
        }
    }

    public List<String> getAllResourcesAndPrivilegesOnTheResourceByRoleId(EMEnum.RoleName roleName)
            throws ResourceNotFoundException {
        List<String> data = new ArrayList<>();

        List<RolePermission> rolePermissions = rolePermissionRepository.getByRoleName(roleName);
        rolePermissions.forEach(rolePermission -> {
            if (rolePermission.getCanEdit() && rolePermission.getCanView()) {
                data.add(EMEnum.ResourceStatus.CAN_EDIT.name() +
                        APIConstants.UNDERSCORE +
                        rolePermission.getResource().getName());
            } else if (rolePermission.getCanView()) {
                data.add(EMEnum.ResourceStatus.CAN_VIEW.name() +
                        APIConstants.UNDERSCORE +
                        rolePermission.getResource().getName());
            } else {
                data.add(EMEnum.ResourceStatus.NON.name() +
                        APIConstants.UNDERSCORE +
                        rolePermission.getResource().getName());
            }
        });
        return data;

    }


}
