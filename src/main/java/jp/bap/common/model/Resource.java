package jp.bap.common.model;

import jp.bap.common.EMEnum;
import jp.bap.common.base.BaseModel;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "resource")
public class Resource extends BaseModel {

    @Column(name = "name")
    @Enumerated(EnumType.STRING)
    private EMEnum.ResourceName name;

    public Resource(){
        super();
    }

    public Resource(EMEnum.ResourceName resourceName) {
        this.name = resourceName;
    }

    //    @OneToMany(mappedBy = "resource")
//    private List<RolePermission> rolePermissions = new ArrayList<>();

}
