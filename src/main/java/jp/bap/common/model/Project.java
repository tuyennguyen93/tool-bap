package jp.bap.common.model;

import jp.bap.common.EMEnum;
import jp.bap.common.base.BaseModel;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;


import javax.persistence.*;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "project")
public class Project extends BaseModel {

    @Column(name = "name")
    private String name;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "description")
    private String description;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private EMEnum.ProjectStatus status;

    @PositiveOrZero
    @Column(name = "number_of_member")
    private Integer numberOfMember;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "pm_id", insertable = false, updatable = false)
    private Employee projectManager;

    @Column(name = "pm_id")
    private Integer projectManagerId;

    @OneToMany(mappedBy = "project")
    private List<MemberProject> memberProjects = new ArrayList<>();

//    @OneToMany(mappedBy = "project")
//    private List<ProjectLeader> projectLeaders = new ArrayList<>();
//
//    @OneToMany(mappedBy = "project")
//    private List<Report> reports =  new ArrayList<>();
}
