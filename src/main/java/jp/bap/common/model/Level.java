package jp.bap.common.model;

import jp.bap.common.base.BaseModel;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "level")
public class Level extends BaseModel {

    @Column(name = "name")
    private String name;

//    @OneToMany(mappedBy = "level")
//    private List<Employee> listEmployees = new ArrayList<>();
}
