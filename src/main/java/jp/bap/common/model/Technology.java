package jp.bap.common.model;

import jp.bap.common.base.BaseModel;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Data
@Table(name = "technology")
public class Technology extends BaseModel {

    @Column(name = "technology_name")
    private String name;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "employee_id", updatable = false, insertable = false)
    private Employee employee;

}
