package jp.bap.common.model;

import jp.bap.common.base.BaseModel;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Data
@Table(name = "salary")
public class Salary extends BaseModel {

    @Column(name = "salary")
    private Double salary;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;

}
