package jp.bap.common.model;

import jp.bap.common.base.BaseModel;
import jp.bap.common.validation.ContactNumberConstraint;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.*;

@Entity
@Data
@Table(name = "employee")
public class Employee extends BaseModel {

    @NotNull(message = "Fullname is not null")
    @Column(name = "full_name")
    private String fullName;

    @NotNull(message = "Date of birth is not null")
    @Past(message = "Date of birth greater than now day")
    @Column(name = "dob")
    private LocalDate dob;

    @NotNull
    @Column(name = "join_date")
    private LocalDate joinDate;

    @Pattern(regexp = "^(09|07|03|08|05)\\d{8}$")
    @Column(name = "phone_number")
    private String phoneNumber;

    @OneToOne(mappedBy = "employee" ,fetch = FetchType.LAZY)
    private User user;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "level_id", updatable = false, insertable = false)
    private Level level;

    @Column(name = "level_id")
    private Integer levelId;

    @Column(name = "salary_present")
    private Double salaryPresent;

//    @OneToMany(mappedBy = "employee")
//    private List<Report> reports =  new ArrayList<>();
//
    @OneToMany(mappedBy = "employee")
    private List<MemberProject> joinProjects = new ArrayList<>();

//    @OneToMany(mappedBy = "employee")
//    private List<ProjectLeader> projectLeaders = new ArrayList<>();
//
    @OneToMany(mappedBy = "projectManager")
    private List<Project> managerProjects = new ArrayList<>();


    @Column(name = "official_date")
    private LocalDate officialDate;

    @OneToMany(mappedBy = "employee")
    private List<Salary> salary = new ArrayList<>();

    @OneToMany(mappedBy = "employee")
    private List<Technology> technology = new ArrayList<>();

}
