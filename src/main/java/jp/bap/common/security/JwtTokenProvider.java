package jp.bap.common.security;

import io.jsonwebtoken.*;
import jp.bap.common.util.constant.MessageConstants;
import jp.bap.common.dto.UserPrincipalDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created on 19/3/2020.
 * Class: JwtTokenProvider.java
 * By : Admin
 */
@Component
public class JwtTokenProvider {
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;

    public String generateToken(Authentication authentication) {

        UserPrincipalDto userPrincipal = (UserPrincipalDto) authentication.getPrincipal();

        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

        return Jwts.builder()
                .setSubject(Long.toString(userPrincipal.getId()))
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public Integer getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return Integer.parseInt(claims.getSubject());
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error(MessageConstants.INVALID_JWT_SIGNATURE);
        } catch (MalformedJwtException ex) {
            logger.error(MessageConstants.INVALID_JWT_TOKEN);
        } catch (ExpiredJwtException ex) {
            logger.error(MessageConstants.EXPIRED_JWT_TOKEN);
        } catch (UnsupportedJwtException ex) {
            logger.error(MessageConstants.UNSUPORT_JWT_TOKEN);
        } catch (IllegalArgumentException ex) {
            logger.error(MessageConstants.JWT_CLAIMS_EMPTY);
        }
        return false;
    }
}
