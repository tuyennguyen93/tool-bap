package jp.bap.common.dto;

import lombok.Data;

import java.util.List;

@Data
public class JwtAuthenticationStatusDto {
    private String accessToken;
    private String tokenType = "Bearer";
    private List<String> permissions;

    public JwtAuthenticationStatusDto(String accessToken, List<String> permissions) {
        this.accessToken = accessToken;
        this.permissions = permissions;
    }
}
