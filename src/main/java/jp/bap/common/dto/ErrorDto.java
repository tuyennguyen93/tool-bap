package jp.bap.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDto {
//    private Object value;
//    private String field;
//    private List<String> errors;

    private String email;
    private List<String> errors;

}
