package jp.bap.common.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ApiStatusDto {

    private Boolean success;
    private String message;
}
