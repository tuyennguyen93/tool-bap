package jp.bap.common.dto;

import lombok.Data;
import org.hibernate.validator.constraints.UniqueElements;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.Date;

@Data
public class SignUpRequestDto {

    @NotBlank
    @Size(min = 4, max = 40,message = "size must be between 4 and 40")
    private String fullName;

    @NotBlank
    @Size(max = 40)
    @Email
    //@UniqueElements
    private String email;

    @NotBlank
    @Size(min = 6, max = 20)
    private String password;

    @Pattern(regexp = "^(09|07|03|08|05)\\d{8}$", message = "phone number validation !")
    private String phoneNumber;

    @NotNull(message = "Date of birth is not null")
    @Past(message = "Date of birth greater than now day")
    private LocalDate dob;

}
