package jp.bap.common.config;

import jp.bap.common.EMEnum;
import jp.bap.common.model.Resource;
import jp.bap.common.model.Role;
import jp.bap.common.model.RolePermission;
import jp.bap.common.repository.ResourceRepository;
import jp.bap.common.repository.RolePermissionRepository;
import jp.bap.common.repository.RoleRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

@Component
@Order(1)
public class SetupDataLoader implements CommandLineRunner {


    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private RolePermissionRepository rolePermissionRepository;


    @Override
    public void run(String... args) throws Exception {
        if (this.roleRepository.count() == 0) {
            List<Role> roles = new ArrayList<>(Arrays.asList(
                    new Role(EMEnum.RoleName.ROLE_ADMIN),
                    new Role(EMEnum.RoleName.ROLE_PM),
                    new Role(EMEnum.RoleName.ROLE_QCQA),
                    new Role(EMEnum.RoleName.ROLE_LEADER),
                    new Role(EMEnum.RoleName.ROLE_MEMBER)
            ));
            this.roleRepository.saveAll(roles);
        }

        if (this.resourceRepository.count() == 0) {

            List<Resource> resources = new ArrayList<>(Arrays.asList(
                    new Resource(EMEnum.ResourceName.USER),
                    new Resource(EMEnum.ResourceName.EMPLOYEE),
                    new Resource(EMEnum.ResourceName.PROJECT),
                    new Resource(EMEnum.ResourceName.REPORT)
            ));
            this.resourceRepository.saveAll(resources);

        }
        if (this.rolePermissionRepository.count() == 0) {
            List<Role> roles = this.roleRepository.findAll();
            List<Resource> resources = this.resourceRepository.findAll();
            List<RolePermission> rolePermissions = new ArrayList<>();
            if (!roles.isEmpty() || !resources.isEmpty()) {
                for (Role role : roles) {
                    if (EMEnum.RoleName.ROLE_ADMIN.equals(role.getName())) {

                        for (Resource resource : resources) {
                            rolePermissions.add(new RolePermission(true, true, role, role.getId(), resource, resource.getId()));
                        }

                    } else {

                        for (Resource resource : resources) {
                            if (EMEnum.ResourceName.USER.equals(resource.getName())) {
                                rolePermissions.add(new RolePermission(false, false, role, role.getId(), resource, resource.getId()));
                            }
                            rolePermissions.add(new RolePermission(true, false, role, role.getId(), resource, resource.getId()));
                        }

                    }
                }
            }
            this.rolePermissionRepository.saveAll(rolePermissions);
        }
    }
}
