package jp.bap.common.exception;

import jp.bap.common.aspect.LoggingAspect;
import jp.bap.common.response.APIResponse;
import jp.bap.common.response.APIResponseError;
import jp.bap.common.util.constant.APIConstants;
import jp.bap.common.util.constant.MessageConstants;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

/**
 * API ExceptionHandler
 */
@ControllerAdvice
public class APIExceptionHandler extends ResponseEntityExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(LoggingAspect.class);

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<FieldError> fieldError = ex.getBindingResult().getFieldErrors();
        APIResponseError apiResponseError = APIResponseError.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .error("Validation Failed")
                .listMessage(fieldError.stream()
                        .map(error -> error.getField() + " : " + error.getDefaultMessage())
                        .collect(Collectors.toList()))
                .build();
        return new ResponseEntity(apiResponseError, HttpStatus.BAD_REQUEST);
    }

    /**
     * Handle {@link ResourceNotFoundException}
     *
     * @param e {@link ResourceNotFoundException}
     * @returm {@link ResponseEntity} of {@link APIResponse}
     */
    @ExceptionHandler(value = {ResourceNotFoundException.class})
    protected ResponseEntity<APIResponseError> handleResourceNotFoundException(ResourceNotFoundException e) {
        log.error(e.getMessage(), e);
        APIResponseError apiResponseError = APIResponseError.builder()
                .code(HttpStatus.NOT_FOUND.value())
                .error(HttpStatus.NOT_FOUND.name())
                .message(e.getMessage())
                .build();
        log.warn(MessageConstants.LOG_LEVEL_WARN + ":" + e.getMessage());
        return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.NOT_FOUND);
    }

    /**
     * Handle {@link BadRequestException}
     *
     * @param e {@link BadRequestException}
     * @returm {@link ResponseEntity} of {@link APIResponse}
     */
    @ExceptionHandler(value = {BadRequestException.class})
    protected ResponseEntity<APIResponseError> handleBadRequestException(BadRequestException e) {
        log.error(e.getMessage(), e);
        APIResponseError apiResponseError = APIResponseError.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .error(e.getMessage())
                .message(e.getMessage())
                .build();
        log.warn(MessageConstants.LOG_LEVEL_WARN + ":" + e.getMessage());
        return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {org.springframework.security.access.AccessDeniedException.class})
    protected ResponseEntity<APIResponseError> handleAccessDeniedException(AccessDeniedException e) {
        log.error(e.getMessage(), e);
        APIResponseError apiResponseError = APIResponseError.builder()
                .code(HttpStatus.FORBIDDEN.value())
                .error(HttpStatus.FORBIDDEN.name())
                .message(e.getMessage())
                .build();
        log.warn(MessageConstants.LOG_LEVEL_WARN + ":" + e.getMessage());
        return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = {AuthenticationException.class})
    protected ResponseEntity<APIResponseError> handleAuthenticationException(AuthenticationException e) {
        log.error(e.getMessage(), e);
        APIResponseError apiResponseError = APIResponseError.builder()
                .code(HttpStatus.UNAUTHORIZED.value())
                .error(HttpStatus.UNAUTHORIZED.name())
                .message(e.getMessage())
                .build();
        log.warn(MessageConstants.LOG_LEVEL_WARN + ":" + e.getMessage());
        return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.UNAUTHORIZED);
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<APIResponseError> handleException(Exception e) {
        log.error(e.getMessage(), e);
        APIResponseError apiResponseError = APIResponseError.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .error(APIConstants.ERROR_UNKOWN)
                .message(!StringUtils.isEmpty(e.getMessage()) ? e.getMessage() : APIConstants.ERROR_UNKNOW_MSG)
                .build();
        return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(FileNotFoundException.class)
    public ResponseEntity<APIResponseError> handleFileException(FileNotFoundException e) {
        APIResponseError apiResponseError = APIResponseError.builder()
                .code(HttpStatus.BAD_REQUEST.value())
                .error(HttpStatus.BAD_REQUEST.name())
                .message(!StringUtils.isEmpty(e.getMessage()) ? e.getMessage() : APIConstants.ERROR_UNKNOW_MSG)
                .build();
        return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.BAD_REQUEST);
    }


    /**
     * Handle {@link PaginationSortingException}
     *
     * @param e {@link PaginationSortingException}
     * @returm {@link ResponseEntity} of {@link APIResponse}
     */
    @ExceptionHandler(value = {PaginationSortingException.class})
    protected ResponseEntity<APIResponseError> handlePaginationSortingException(PaginationSortingException e) {
        log.error(e.getMessage(), e);
        APIResponseError apiResponseError = APIResponseError.builder()
                .code(HttpStatus.PRECONDITION_FAILED.value())
                .error(e.getMessage())
                .message(e.getMessage())
                .build();
        log.warn(MessageConstants.LOG_LEVEL_WARN + ":" + e.getMessage());
        return new ResponseEntity<APIResponseError>(apiResponseError, HttpStatus.PRECONDITION_FAILED);
    }
}
