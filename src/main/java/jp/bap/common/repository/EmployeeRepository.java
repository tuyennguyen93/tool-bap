package jp.bap.common.repository;

import jp.bap.common.EMEnum;
import jp.bap.common.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    @Query("SELECT c FROM Employee c "
            +"JOIN c.joinProjects m "
            +"WHERE  m.projectId = :projectId")
    Page<Employee> getEmployeeByProjectForAdmin(@Param("projectId") Integer projectId, Pageable pageable);


    @Query("SELECT c FROM Employee c "
            +"JOIN c.joinProjects m "
            +"JOIN m.project p "
            +"WHERE  p.id = :projectId AND p.projectManagerId = :pmId ")
    Page<Employee> getEmployeeByProjectForPM(@Param("projectId") Integer projectId,
                                             @Param("pmId") Integer pmId,
                                             Pageable pageable);


    /* Query Get By PM Id*/
    @Query("SELECT DISTINCT c FROM Employee c "
            +"JOIN c.joinProjects m  "
            +"JOIN m.project p "
            +"WHERE p.projectManagerId = :pmId "
            + "AND c.id = :Id  ")
    Optional<Employee> getEmployeeOfPMById(@Param("pmId") Integer pmId,
                                           @Param("Id") Integer Id);

    /* Query Get By PM */
    @Query("SELECT DISTINCT c FROM Employee c "
            +"JOIN c.joinProjects m "
            +"JOIN m.project p "
            +"WHERE p.projectManagerId = :pmId ")
    Page<Employee> getEmployeeByPM(@Param("pmId") Integer pmId ,Pageable pageable);


    /* Query Get By MEMBER, QAQC */
    @Query("SELECT c FROM Employee c " +
            "WHERE c.id = :id")
    Page<Employee> getEmployeeByMember(@Param("id") Integer id , Pageable pageable);


    /* Query Search Admin */
    @Query(value = "SELECT DISTINCT c "
            + "FROM Employee c "
            + "LEFT JOIN c.joinProjects m "
            + "LEFT JOIN m.project p "
            + "LEFT JOIN c.user u "
            + "LEFT JOIN u.role r "
            + "LEFT JOIN c.technology t "
            + "LEFT JOIN c.salary s "
            + "WHERE(:fullName IS NULL OR :fullName = '' OR c.fullName LIKE CONCAT('%',:fullName,'%')) "
            + "AND (:projectName IS NULL OR :projectName = '' OR  p.name LIKE CONCAT('%',:projectName,'%')) "
            + "AND (:email IS NULL OR :email = '' OR  u.email LIKE CONCAT('%',:email,'%')) "
            + "AND (:roleSearch IS NULL OR :roleSearch = '' OR  r.name LIKE :roleSearch) "
            + "AND (:staff IS NULL OR :staff = '' OR ( 1 = :staff  and c.officialDate is not null) OR (2 =:staff and c.officialDate is null)  ) "
            + "AND (:salaryFrom = -1 OR  :salaryTo = -1 OR (c.salaryPresent <= :salaryTo and c.salaryPresent >= :salaryFrom ) )"
            + "AND ( (t.name in :techs )) "
    )
    Page<Employee> findAllCustomerByAdmin(@Param("fullName") String fullName,
                                          @Param("projectName") String projectName,
                                          @Param("email") String email,
                                          @Param("staff") String staff,
                                          @Param("roleSearch") String roleSearch,
                                          @Param("salaryFrom") Integer salaryFrom,
                                          @Param("salaryTo") Integer salaryTo,
                                          Pageable pageable,
                                          @Param("techs") String ...techs);

    @Query(value = "SELECT DISTINCT c "
            + "FROM Employee c "
            + "LEFT JOIN c.joinProjects m "
            + "LEFT JOIN m.project p "
            + "LEFT JOIN c.user u "
            + "LEFT JOIN u.role r "
            + "LEFT JOIN c.technology t "
            + "LEFT JOIN c.salary s "
            + "WHERE(:fullName IS NULL OR :fullName = '' OR c.fullName LIKE CONCAT('%',:fullName,'%')) "
            + "AND (:projectName IS NULL OR :projectName = '' OR  p.name LIKE CONCAT('%',:projectName,'%')) "
            + "AND (:email IS NULL OR :email = '' OR  u.email LIKE CONCAT('%',:email,'%')) "
            + "AND (:roleSearch IS NULL OR :roleSearch = '' OR  r.name LIKE :roleSearch) "
            + "AND (:staff IS NULL OR :staff = '' OR ( 1 = :staff  and c.officialDate is not null) OR (2 =:staff and c.officialDate is null)  ) "
            + "AND (:salaryFrom = -1 OR  :salaryTo = -1 OR (c.salaryPresent <= :salaryTo and c.salaryPresent >= :salaryFrom ) )"
    )
    Page<Employee> findAllCustomerByAdminNoTech(@Param("fullName") String fullName,
                                          @Param("projectName") String projectName,
                                          @Param("email") String email,
                                          @Param("staff") String staff,
                                          @Param("roleSearch") String roleSearch,
                                          @Param("salaryFrom") Integer salaryFrom,
                                          @Param("salaryTo") Integer salaryTo,
                                          Pageable pageable);


    /* Query Search PM */
    @Query(value = "SELECT DISTINCT c "
            + "FROM Employee c "
            + "LEFT JOIN c.joinProjects m "
            + "LEFT JOIN m.project p "
            + "LEFT JOIN c.user u "
            + "LEFT JOIN u.role r "
            + "LEFT JOIN c.technology t "
            + "LEFT JOIN c.salary s "
            + "WHERE(:fullName IS NULL OR :fullName = '' OR c.fullName LIKE CONCAT('%',:fullName,'%')) "
            + "AND (:projectName IS NULL OR :projectName = '' OR  p.name LIKE CONCAT('%',:projectName,'%')) "
            + "AND (:email IS NULL OR :email = '' OR  u.email LIKE CONCAT('%',:email,'%')) "
            + "AND (:roleSearch IS NULL OR :roleSearch = '' OR  r.name LIKE :roleSearch) "
            + "AND (:staff IS NULL OR :staff = '' OR ( 1 = :staff  and c.officialDate is not null) OR (2 =:staff and c.officialDate is null)  ) "
            + "AND (:salaryFrom = -1 OR  :salaryTo = -1 OR (c.salaryPresent <= :salaryTo and c.salaryPresent >= :salaryFrom ))"
            + "AND p.projectManagerId = :pmId "
            + "AND t.name in :techs "
    )
    Page<Employee> findProjectManager(@Param("fullName") String fullName,
                                      @Param("projectName") String projectName,
                                      @Param("email") String email,
                                      @Param("pmId") Integer pmId,
                                      @Param("staff") String staff,
                                      @Param("roleSearch") String roleSearch,
                                      @Param("salaryFrom") Integer salaryFrom,
                                      @Param("salaryTo") Integer salaryTo,
                                      Pageable pageable,
                                      @Param("techs") String ...techs);

    @Query(value = "SELECT DISTINCT c "
            + "FROM Employee c "
            + "LEFT JOIN c.joinProjects m "
            + "LEFT JOIN m.project p "
            + "LEFT JOIN c.user u "
            + "LEFT JOIN u.role r "
            + "LEFT JOIN c.technology t "
            + "LEFT JOIN c.salary s "
            + "WHERE(:fullName IS NULL OR :fullName = '' OR c.fullName LIKE CONCAT('%',:fullName,'%')) "
            + "AND (:projectName IS NULL OR :projectName = '' OR  p.name LIKE CONCAT('%',:projectName,'%')) "
            + "AND (:email IS NULL OR :email = '' OR  u.email LIKE CONCAT('%',:email,'%')) "
            + "AND (:roleSearch IS NULL OR :roleSearch = '' OR  r.name LIKE :roleSearch) "
            + "AND (:staff IS NULL OR :staff = '' OR ( 1 = :staff  and c.officialDate is not null) OR (2 =:staff and c.officialDate is null)  ) "
            + "AND (:salaryFrom = -1 OR  :salaryTo = -1 OR (c.salaryPresent <= :salaryTo and c.salaryPresent >= :salaryFrom ))"
            + "AND p.projectManagerId = :pmId "
    )
    Page<Employee> findProjectManagerNoTech(@Param("fullName") String fullName,
                                      @Param("projectName") String projectName,
                                      @Param("email") String email,
                                      @Param("pmId") Integer pmId,
                                      @Param("staff") String staff,
                                      @Param("roleSearch") String roleSearch,
                                      @Param("salaryFrom") Integer salaryFrom,
                                      @Param("salaryTo") Integer salaryTo,
                                      Pageable pageable);



    @Query("SELECT c FROM Employee c  " +
            "JOIN c.user u " +
            "WHERE u.id = :id")
    List<Employee> isExistEmployee(@Param("id") Integer userId);

    Employee findByUserId(Integer user_id);

    @Query("SELECT e FROM Employee e "
            + "JOIN  e.user u "
            + "JOIN  u.role r "
            + "WHERE r.name = :roleName AND u.isActive = true")
    List<Employee> findEmployeeByRoleName(@Param("roleName") EMEnum.RoleName roleName);

}
