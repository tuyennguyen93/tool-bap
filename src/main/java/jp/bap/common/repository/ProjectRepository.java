package jp.bap.common.repository;

import jp.bap.common.EMEnum;
import jp.bap.common.model.Employee;
import jp.bap.common.model.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {

    List<Project> findByProjectManagerIdAndStatus(Integer projectManagerId, EMEnum.ProjectStatus status);


    Page<Project> findByProjectManagerId(Integer id, Pageable pageable);

    @Query("SELECT p FROM Project p JOIN p.memberProjects mB " +
            "WHERE mB.employeeId = :id AND mB.getOutDate IS NULL")
    Page<Project> findProjectByMemberId(@Param("id") Integer id, Pageable pageable);

    boolean existsByName(String name);

    @Query("SELECT p FROM Project p WHERE :name ='' OR :name IS NULL OR p.name LIKE CONCAT('%',:name,'%')")
    Page<Project> findProjectRoleAdmin(@Param("name") String name, Pageable pageable);

    @Query("SELECT p FROM Project p WHERE p.projectManagerId = :pMId " +
            "AND (:name ='' OR :name IS NULL OR p.name LIKE CONCAT('%',:name,'%'))")
    Page<Project> findProjectRolePM(@Param("name") String name, @Param("pMId") Integer pMId, Pageable pageable);

   /* @Query("SELECT p FROM Project p JOIN MemberProject mB on p.id = mB.projectId  WHERE  mB.employeeId = :leaderId " +
            "AND (:name ='' OR :name IS NULL OR p.name LIKE CONCAT('%',:name,'%'))")
    Page<Project> findProjectRoleLeader(@Param("name") String name, @Param("leaderId") Integer leaderId, Pageable pageable);*/

    @Query("SELECT p FROM Project p JOIN MemberProject mB on p.id = mB.projectId  WHERE  mB.employeeId = :memberId " +
            "AND (:name ='' OR :name IS NULL OR p.name LIKE CONCAT('%',:name,'%'))")
    Page<Project> findProjectRoleMember(@Param("name") String name, @Param("memberId") Integer memberId, Pageable pageable);


    @Query("SELECT p FROM Project p JOIN p.memberProjects mB  WHERE p.status = 'ENABLE' AND mB.employeeId = :employeeId")
    List<Project> findByEmployeeId(@Param("employeeId") Integer employeeId);

    Boolean existsByIdAndStatus(Integer id, EMEnum.ProjectStatus status);
}
