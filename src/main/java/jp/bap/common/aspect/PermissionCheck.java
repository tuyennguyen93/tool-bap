package jp.bap.common.aspect;


import jp.bap.common.EMEnum;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface PermissionCheck {
    EMEnum.ResourceName resource();
    boolean canView() default false;
    boolean canEdit() default false;
}
